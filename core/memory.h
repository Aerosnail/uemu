#pragma once

#include <stdint.h>
#include <stdlib.h>

struct mem_handler {
	int (*read) (void *ctx, uint64_t addr, void *data, size_t len);
	int (*write) (void *ctx, uint64_t addr, const void *data, size_t len);
	int (*execute) (void *ctx, uint64_t addr, void *instr, size_t len);

	void *ctx;
};

typedef struct bus MemBus;
typedef struct mem_entry MemEntry;

/**
 * Initialize memory address space handler
 */
void mem_init(void);

/**
 * Denitialize memory address space handler
 *
 * @return 0 on success, errno on failure
 */
int mem_deinit(void);

/**
 * Create a new memory address space
 *
 * @param bus  pointer to new bus object, will be filled by the funcntion
 * @param name address space name, used for indexing with mem_get_bus
 *
 * @return pointer to the new address space on success, NULL on failure
 */
int mem_create_bus(MemBus **bus, const char *name);

/**
 * Delete a memory bus address space
 *
 * @param bus   pointer to bus object
 *
 * @return 0 on success, errno on failure
 */
int mem_delete_bus(MemBus **bus);

MemBus* mem_get_bus(const char *name);

/**
 * Register a new handler for a given address space
 */
MemEntry* mem_register(MemBus *mgr, const struct mem_handler *handler, uint64_t start_addr, uint64_t len);
int mem_unregister(MemBus *self, MemEntry *handle);

/**
 * Perform a read in the given address space
 *
 * @param mgr       address space handler
 * @param address   address to read
 * @param data      buffer to fill with data
 * @param len       number of bytes to read
 *
 * @return 0 on success, errno on failure
 */
int mem_read(MemBus *mgr, uint64_t addr, void *data, size_t len);

/**
 * Perform a write in the given address space
 *
 * @param mgr       address space handler
 * @param address   address to write
 * @param data      data to write
 * @param len       number of bytes to write
 *
 * @return 0 on success, errno on failure
 */
int mem_write(MemBus *mgr, uint64_t addr, const void *data, size_t len);

/**
 * Perform a read in the given address space, with the intent of executing the
 * bytes returned
 *
 * @param mgr       address space handler
 * @param address   address to read
 * @param data      buffer to fill with data
 * @param len       number of bytes to read
 *
 * @return 0 on success, errno on failure
 */
int mem_execute(MemBus *mgr, uint64_t addr, void *instr, size_t len);
