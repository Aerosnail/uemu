#pragma once

#include <stdlib.h>

int gdb_format_packet(void *dst, size_t *dst_len, const void *src, size_t src_len, int ack);
