#pragma once

#include <stdint.h>
#include <stdlib.h>

struct gdbstub;

enum gdbstub_breakpoint_type {
	GDBSTUB_BKPT_SW = 0,
	GDBSTUB_BKPT_HW = 1,
	GDBSTUB_BKPT_WATCH_WRITE = 2,
	GDBSTUB_BKPT_WATCH_READ = 3,
	GDBSTUB_BKPT_WATCH_ACCESS = 4,
};

typedef int (*gdb_fcn_t)(void *ctx, const char *args, size_t len, char *resp, size_t resplen);

int gdbstub_init(struct gdbstub **handle, uint16_t port);
int gdbstub_deinit(struct gdbstub **handle);
int gdbstub_notify(struct gdbstub *stub, const char *cmd, size_t len);

int gdbstub_register_cmd(struct gdbstub *stub, const char *cmd, gdb_fcn_t fcn, void *ctx);
int gdbstub_unregister_cmd(struct gdbstub *stub, const char *cmd);

