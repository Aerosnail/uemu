#pragma once

#include <stdint.h>
#include <stdlib.h>

struct gdb_parser;

int gdb_parser_init(struct gdb_parser **parser, size_t maxlen);
int gdb_parser_deinit(struct gdb_parser **parser);

char* gdb_parser_feed(struct gdb_parser *parser, char byte);
int   gdb_parser_reset(struct gdb_parser *parser);
