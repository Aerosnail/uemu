#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include "formatter.h"

int
gdb_format_packet(void *vdst, size_t *dst_len, const void *vsrc, size_t src_len, int ack)
{
	const char *src = (const char*)vsrc;
	char *dst = (char*)vdst;

	size_t pkt_len;
	size_t i, j;
	uint8_t crc;

	/* Compute the output packet len */
	pkt_len = 0;
	if (ack) pkt_len++;    /* + */
	pkt_len++;             /* $ */

	for (i = 0; i < src_len && src[i]; i++) {
		switch (src[i]) {
		case '$':
		case '#':
		case '*':
		case '{':
			/* Escape sequence */
			pkt_len += 2;
			break;

		default:
			/* Regular byte */
			pkt_len++;
			break;
		}
	}

	/* Update src length to the actual length of the response */
	src_len = i;


	pkt_len++;              /* # */
	pkt_len += 2;           /* CRC */

	if (pkt_len > *dst_len) return ENOMEM;

	/* Write packet back-to-front so that dst dst and src can overlap */
	j = pkt_len - 1;
	j -= 2;             /* Leave space for CRC */

	crc = 0;
	dst[j--] = '#';
	for (i = src_len - 1; i + 1 > 0; i--) {
		switch (src[i]) {
		case '$':
		case '#':
		case '*':
		case '{':
			/* Escape sequence */
			crc += ('{' + src[i]) ^ 0x20;
			dst[j--] = src[i] ^ 0x20;
			dst[j--] = '{';
			break;

		default:
			/* Regular byte */
			crc += src[i];
			dst[j--] = src[i];
			break;
		}
	}

	assert(j < 2);
	dst[j--] = '$';
	if (ack) {
		assert(j == 0);
		dst[j] = '+';
	}

	/* Append CRC and null terminator */
	sprintf(dst + pkt_len - 2, "%02x", crc);
	*dst_len = pkt_len;

	return 0;
}
