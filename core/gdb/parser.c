#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sys/socket.h>
#include <log/log.h>

#include "parser.h"

struct gdb_parser {
	enum {
		WAIT_FOR_START,
		READ_DATA,
		READ_CRC_0,
		READ_CRC_1,
		DATA_END
	} state;

	int escape_next, rle_next;
	uint8_t crc, rx_crc;
	size_t len, maxlen;
	char buf[];
};

static int hexchar_to_int(const char digit);

int
gdb_parser_init(struct gdb_parser **parser, size_t maxlen)
{
	if (!parser) return EINVAL;
	if (!maxlen) return EINVAL;

	*parser = calloc(1, sizeof(**parser) + maxlen);
	if (!*parser) return ENOMEM;

	(*parser)->maxlen = maxlen;

	return gdb_parser_reset(*parser);
}

int gdb_parser_deinit(struct gdb_parser **parser)
{
	if (!parser) return EINVAL;
	if (!*parser) return EINVAL;

	free(*parser);
	*parser = NULL;

	return 0;
}

int
gdb_parser_reset(struct gdb_parser *parser)
{
	if (!parser) return EINVAL;

	parser->state = WAIT_FOR_START;
	parser->len = 0;
	parser->rle_next = 0;
	parser->escape_next = 0;
	parser->crc = 0;
	parser->rx_crc = 0;

	return 0;
}

char*
gdb_parser_feed(struct gdb_parser *parser, char byte_in)
{
	int i;
	uint8_t rle_byte;

	if (!parser) return NULL;

	switch (parser->state) {
	case WAIT_FOR_START:
		switch (byte_in) {
		case '-':
			/* NACK */
			return parser->buf;
		case '$':
			/* Packet start received */
			gdb_parser_reset(parser);
			parser->state = READ_DATA;
			break;

		case 0x03:
			log_debug("BREAK");
			return "BREAK";
			break;

		default:
			break;
		}
		break;

	case READ_DATA:
		if (parser->escape_next) {
			/* Un-escape character */
			parser->buf[parser->len++] = byte_in ^ 0x20;
			parser->escape_next = 0;
			parser->crc += byte_in;
		} else if (parser->rle_next) {
			rle_byte = parser->buf[parser->len - 1];

			/* Expand RLE runs */
			for (i = 29; i < byte_in; i++) {
				parser->buf[parser->len++] = rle_byte;
			}
			parser->rle_next = 0;
			parser->crc += byte_in;
		} else {
			/* Parse as a regular character */
			switch (byte_in) {
			case '$':   /* Packet start */
				parser->state = READ_DATA;
				parser->len = 0;
				parser->escape_next = 0;
				parser->rle_next = 0;
				parser->crc = 0;
				parser->rx_crc = 0;
				break;

			case '#':   /* Packet end */
				parser->buf[parser->len] = '\0';
				parser->state = READ_CRC_0;
				break;

			case '}':   /* Escape sequence */
				parser->escape_next = 1;
				parser->crc += byte_in;
				break;

			case '*':   /* RLE */
				parser->rle_next = 1;
				parser->crc += byte_in;
				break;

			default:    /* Regular byte */
				parser->buf[parser->len++] = byte_in;
				parser->crc += byte_in;
				break;
			}
		}
		break;

	case READ_CRC_0:
		parser->rx_crc = hexchar_to_int(byte_in) << 4;
		parser->state = READ_CRC_1;
		break;

	case READ_CRC_1:
		parser->rx_crc |= hexchar_to_int(byte_in);
		parser->state = DATA_END;
		break;

	default:
		log_warn("Unknown state %d, resetting...", parser->state);
		gdb_parser_reset(parser);
		break;
	}

	if (parser->state == DATA_END) {
		/* Buffer now contains a full packet */

		parser->state = WAIT_FOR_START;

		if (parser->rx_crc == parser->crc) {
			return parser->buf;
		}

	}

	return NULL;
}


static int
hexchar_to_int(const char digit)
{
	if (digit >= '0' && digit <= '9')
		return digit - '0';
	if (digit >= 'A' && digit <= 'F')
		return digit - 'A' + 10;
	if (digit >= 'a' && digit <= 'f')
		return digit - 'a' + 10;
	return -1;
}
