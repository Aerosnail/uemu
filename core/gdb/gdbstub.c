#include <assert.h>
#include <linux/tcp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include <log/log.h>
#include <utils.h>

#include "gdbstub.h"
#include "parser.h"
#include "formatter.h"

#define MAX_CMD_LEN 0x8000
#define SENDRECV_MAX_CHUNKSIZE 256
#define MAX_CONCURRENT_CONNECTIONS 1

struct gdbcmd_handler {
	const char *prefix;
	void *ctx;
	gdb_fcn_t fcn;
};

struct ipcmsg {
	enum {
		QUIT,
		NOTIFY
	} type;

	union {
		struct {
			char *msg;
			size_t len;
		} notify;
	} data;
};

struct worker_args {
	int sockfd;
	int ipcfd;

	struct gdbstub *stub;
};

struct gdbstub {
	int ipcfd;
	pthread_t tid;

	struct gdbcmd_handler *handlers;
	size_t handler_count;
};

static int execute_command(struct gdbcmd_handler *handlers, size_t count, const char *cmd, char *resp, size_t resplen);
static void* gdbstub_listener(void *args);
static int send_packet(int fd, char *sendbuf, size_t sendlen, const char *src, int ack);
static void* get_in_addr(struct sockaddr *sa);

__global int
gdbstub_init(struct gdbstub **handle, uint16_t port)
{
	struct addrinfo hints, *res;
	char port_str[5 + 1];
	int reuse = 1;
	int ret;
	int listen_sockfd;
	int listener_pipe[2];
	struct worker_args *worker_args;

	if (!handle) return EINVAL;

	*handle = calloc(1, sizeof(**handle));
	if (!*handle) return ENOMEM;

	/* Describe a socket bound to localhost (any IP version) on the specified
	 * port */
	snprintf(port_str, sizeof(port_str), "%hu", port);
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	ret = getaddrinfo(NULL, port_str, &hints, &res);
	if (ret) goto failure;

	/* Create socket */
	listen_sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (listen_sockfd < 0) {
		ret = EAFNOSUPPORT;
		goto failure;
	}

	/* Allow rebinding to the same port in the future */
	setsockopt(listen_sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

	/* Bind socket */
	if (bind(listen_sockfd, res->ai_addr, res->ai_addrlen) < 0) {
		log_error("Bind failed: %s", strerror(errno));
		ret = errno;
		goto failure;
	}

	/* Listen for incoming connections */
	if (listen(listen_sockfd, MAX_CONCURRENT_CONNECTIONS)) {
		log_error("Listen failed: %s", strerror(errno));
		ret = errno;
		goto failure;
	}

	/* Create pipe to signal the worker to exit */
	if (pipe(listener_pipe) < 0) {
		log_error("%s", strerror(errno));
		ret = errno;
		goto failure;
	}
	(*handle)->ipcfd = listener_pipe[1];

	/* Launch listener worker */
	worker_args = calloc(1, sizeof(*worker_args));
	worker_args->sockfd = listen_sockfd;
	worker_args->ipcfd = listener_pipe[0];
	worker_args->stub = *handle;
	ret = pthread_create(&(*handle)->tid, NULL, gdbstub_listener, worker_args);

	freeaddrinfo(res);

	/* Do not crash if sockets close unexpectedly (error handling around all
	 * socket calls handles it already) */
	signal(SIGPIPE, SIG_IGN);

	return ret;

failure:
	freeaddrinfo(res);
	if ((*handle)->ipcfd) close((*handle)->ipcfd);
	free(*handle);
	*handle = NULL;
	return ret;
}

__global int
gdbstub_deinit(struct gdbstub **handle)
{
	struct ipcmsg quit = { .type = QUIT };
	int ret;

	if (!handle) return EINVAL;
	if (!*handle) return EINVAL;
	if (!(*handle)->tid) return ECHILD;

	/* Signal worker to terminate */
	ret = write((*handle)->ipcfd, &quit, sizeof(quit));
	if (ret <= (ssize_t)sizeof(quit)) {
		log_warn("Failed to notify worker");
	}

	/* Wait for thread to exit */
	pthread_join((*handle)->tid, NULL);
	(*handle)->tid = 0;

	/* Clean up pipe to listener thread */
	close((*handle)->ipcfd);

	/* Clean up command list */
	free((*handle)->handlers);
	(*handle)->handlers = NULL;

	free(*handle);
	*handle = NULL;

	return 0;
}
__global int
gdbstub_notify(struct gdbstub *stub, const char *message, size_t len)
{
	struct ipcmsg ipcmsg;
	int ret;

	if (!stub) return EINVAL;
	if (!stub->ipcfd) return EPIPE;

	ipcmsg.type = NOTIFY;
	ipcmsg.data.notify.msg = strdup(message);
	ipcmsg.data.notify.len = len;


	ret = write(stub->ipcfd, &ipcmsg, sizeof(ipcmsg));
	if (ret <= (ssize_t)sizeof(ipcmsg)) {
		return EPIPE;
	}

	return 0;
}

__global int
gdbstub_register_cmd(struct gdbstub *self, const char *cmd, gdb_fcn_t fcn, void *ctx)
{
	struct gdbcmd_handler *reallocd;
	self->handler_count++;
	reallocd = realloc(self->handlers, sizeof(self->handlers[0]) * self->handler_count);

	if (!reallocd) return ENOMEM;
	self->handlers = reallocd;

	self->handlers[self->handler_count - 1].prefix = cmd;
	self->handlers[self->handler_count - 1].fcn = fcn;
	self->handlers[self->handler_count - 1].ctx = ctx;

	return 0;
}

__global int
gdbstub_unregister_cmd(struct gdbstub *self, const char *cmd)
{
	size_t i;

	for (i = 0; i < self->handler_count; i++) {
		if (!strcmp(cmd, self->handlers[i].prefix)) {
			self->handlers[i] = self->handlers[self->handler_count - 1];
			self->handler_count--;

			if (self->handler_count) {
				self->handlers = realloc(self->handlers, sizeof(self->handlers[0]) * self->handler_count);
			} else {
				free(self->handlers);
				self->handlers = NULL;
			}
			return 0;
		}
	}

	return ENOKEY;
}

/* Static functions {{{ */
static void*
gdbstub_listener(void *vargs)
{
	struct worker_args *args = (struct worker_args*)vargs;
	const size_t sendbuf_len = 2 + 2*MAX_CMD_LEN + 1 + 2;
	struct sockaddr_storage remote_addr;
	socklen_t remote_addr_size;
	struct pollfd poll_fds[2];      /* To gracefully cancel accept() */
	int conn_sockfd;
	char remote_ip[INET6_ADDRSTRLEN];
	struct ipcmsg ipcmsg;

	struct gdb_parser *parser = NULL;
	int sendack;

	uint8_t rx_raw;
	char *sendbuf = NULL;
	char *cmd;
	int ret;

	if (gdb_parser_init(&parser, MAX_CMD_LEN)) {
		log_error("Failed to allocate command parser");
		goto exit;
	}

	/* Over-allocate send buffer to contain a packet full of characters to
	 * escape */
	sendbuf = calloc(sendbuf_len, sizeof(*sendbuf));
	if (!sendbuf) {
		log_error("Failed to allocate response buffer");
		goto exit;
	}

	for (;;) {
		poll_fds[0].fd = args->sockfd;
		poll_fds[0].events = POLLIN;
		poll_fds[1].fd = args->ipcfd;
		poll_fds[1].events = POLLIN;

		/* Wait for an incoming connection */
		do {
			poll(poll_fds, LEN(poll_fds), -1);

			if (poll_fds[1].revents) {
				if (read(args->ipcfd, &ipcmsg, sizeof(ipcmsg)) < (ssize_t)sizeof(ipcmsg)) {
					log_error("IPC communication broke, exiting...");
					goto exit;
				}

				switch (ipcmsg.type) {
				case NOTIFY:
					log_debug("No gdbserver connected, dropping message");
					free(ipcmsg.data.notify.msg);
					break;

				case QUIT:
				default:
					log_debug("Cancellation request received, exiting...");
					goto exit;

				}
			}
		} while(!poll_fds[0].revents);

		/* Accept new client connection */
		remote_addr_size = sizeof(remote_addr);
		conn_sockfd = accept(args->sockfd,
							 (struct sockaddr*)&remote_addr,
							 &remote_addr_size);

		if (conn_sockfd < 0) {
			log_error("Socket accept failed: %s", strerror(errno));
			goto exit;
		}

		inet_ntop(remote_addr.ss_family,
				  get_in_addr((struct sockaddr*)&remote_addr),
				  remote_ip, sizeof(remote_ip));
		log_debug("Accepting connection from %s", remote_ip);

		/* Handle incoming data */
		sendack = 1;
		for (;;) {
			/* Wait for data to become available */
			poll_fds[0].fd = conn_sockfd;
			poll_fds[0].events = POLLIN;
			poll_fds[1].fd = args->ipcfd;
			poll_fds[1].events = POLLIN;

			do {
				poll(poll_fds, LEN(poll_fds), -1);

				if (poll_fds[1].revents) {
					/* Handle IPC event */
					if (read(args->ipcfd, &ipcmsg, sizeof(ipcmsg)) < (ssize_t)sizeof(ipcmsg)) {
						log_error("IPC communication broke, exiting...");
						goto exit;
					}

					switch (ipcmsg.type) {
					case NOTIFY:
						send_packet(conn_sockfd, sendbuf, sendbuf_len, ipcmsg.data.notify.msg, sendack > 0);
						free(ipcmsg.data.notify.msg);
						break;

					case QUIT:
					default:
						log_debug("Cancellation request received, exiting...");
						goto exit;

					}
				}
			} while(!poll_fds[0].revents);

			/* Receive a byte */
			if (recv(conn_sockfd, &rx_raw, 1, 0) <= 0) {
				log_debug("Connection closed");
				break;
			}

			/* Feed the parser, see if enough valid bytes have been collected
			 * to make a full packet */
			cmd = gdb_parser_feed(parser, rx_raw);
			if (!cmd) continue;

			if (sendack > 0 && !strcmp(cmd, "QStartNoAckMode")) {
				/* Stop sending ACKs from now on, reply with OK */
				sendack = 0;
				ret = 0;
				sprintf(sendbuf, "OK");
			} else  {
				/* Execute command */
				ret = execute_command(args->stub->handlers,
			                      	args->stub->handler_count,
			                      	cmd, sendbuf, MAX_CMD_LEN);
			}

			/* Prepare response */
			switch (ret) {
			case 0:         /* Command execution OK: pass response along */
				break;

			case ENOSYS:    /* "Not implemented": reply with an empty string */
				sendbuf[0] = 0;
				break;

			default:        /* Command failed to execute: reply with error code */
				sprintf(sendbuf, "E%d", ret);
				break;
			}

			if (ret != ENOMSG) {
				/* Respond */
				send_packet(conn_sockfd, sendbuf, sendbuf_len, sendbuf, sendack >= 0);

				if (sendack == 0) {
					/* Don't send any more acks */
					sendack--;
				}
			}

			/* Reset parser */
			gdb_parser_reset(parser);
		}

		close(conn_sockfd);
	}

exit:
	close(args->ipcfd);
	close(args->sockfd);
	gdb_parser_deinit(&parser);
	free(sendbuf);
	free(args);

	return NULL;
}

static int
send_packet(int fd, char *sendbuf, size_t sendlen, const char *src, int ack)
{
	int offset;
	int bytes_sent;

	gdb_format_packet(sendbuf, &sendlen, src, -1ULL, ack);

	/* Send response */
	offset = 0;
	while (sendlen) {
		bytes_sent = send(fd, sendbuf + offset, sendlen, 0);

		if (bytes_sent < 0) {
			log_debug("Connection closed: %s", strerror(errno));
			return ENOTCONN;
		}

		sendlen -= bytes_sent;
		offset += bytes_sent;
	}

	return 0;
}

static int
execute_command(struct gdbcmd_handler *handlers, size_t count, const char *cmd, char *resp, size_t resplen)
{
	size_t prefix_len;
	size_t i;

	for (i = 0; i < count; i++) {
		if (strbegins(cmd, handlers[i].prefix)) {
			prefix_len = strlen(handlers[i].prefix);
			return handlers[i].fcn(handlers[i].ctx, cmd + prefix_len,
			                       strlen(cmd) - prefix_len, resp, resplen);
		}
	}

	return ENOSYS;
}

static void*
get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/* }}} */
