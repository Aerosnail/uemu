#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <log/log.h>
#include <utils.h>

#include "memory.h"
#include "log/log.h"

struct mem_entry {
	struct mem_handler *handler;
	uint64_t start_addr;
	uint64_t end_addr;
};

struct bus {
	struct mem_entry **handlers;
	struct mem_entry read_cache, write_cache, exec_cache;
	size_t count;
	size_t count_pow2;

	char *name;
};

static struct mem_entry *mem_find(MemBus *self, uint64_t start_addr);
static void mem_entry_free(struct mem_entry *ptr);

static struct {
	struct bus **buses;
	size_t count;
} _state;

__global void
mem_init(void)
{
	_state.buses = NULL;
	_state.count = 0;
}

__global int
mem_deinit(void)
{
	struct bus *bus;
	size_t i;

	for (i=0; i<_state.count; i++) {
		bus = _state.buses[i];

		assert(!bus->handlers);
		free(bus->name);
		free(bus);
	}

	free(_state.buses);
	_state.count = 0;

	return 0;
}

__global MemBus*
mem_get_bus(const char *name)
{
	size_t i;

	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.buses[i]->name, name)) {
			return _state.buses[i];
		}
	}

	return NULL;
}

__global int
mem_create_bus(MemBus **bus, const char *name)
{
	MemBus *self;

	if (!bus) return EINVAL;
	if ((self = mem_get_bus(name))) return EEXIST;

	self = calloc(1, sizeof(*self));
	if (!self) return ENOMEM;

	_state.count++;
	_state.buses = realloc(_state.buses, _state.count * sizeof(*_state.buses));
	_state.buses[_state.count - 1] = self;

	self->name = strdup(name);
	self->handlers = NULL;
	self->count = 0;
	self->count_pow2 = 1;

	log_debug("Created memory bus \"%s\"", name);

	*bus = self;
	return 0;
}

__global int
mem_delete_bus(MemBus **bus)
{
	size_t i;

	if (!bus || !*bus) return EINVAL;

	for (i=0; i<_state.count; i++) {
		if (_state.buses[i] == *bus) {
			_state.count--;
			_state.buses[i] = _state.buses[_state.count];

			if (_state.count) {
				_state.buses = realloc(_state.buses, _state.count * sizeof(*_state.buses));
			} else {
				free(_state.buses);
				_state.buses = NULL;
			}

			assert(!(*bus)->handlers);
			free((*bus)->name);
			free(*bus);
			*bus = NULL;
			return 0;
		}
	}

	log_error("Failed to find address space '%s'", (*bus)->name);
	return ENOKEY;
}

__global struct mem_entry*
mem_register(MemBus *self, const struct mem_handler *handler, uint64_t start_addr, size_t len)
{
	struct mem_entry *new;
	size_t i;

	log_debug("%p -> %08"PRIx64"..%08"PRIx64,
			handler,
			start_addr,
			start_addr + len - 1);

	new = calloc(1, sizeof(*new));
	if (!new) return NULL;

	/* Populate new entry */
	new->handler = calloc(1, sizeof(*new->handler));
	if (!new->handler) return NULL;

	*new->handler = *handler;
	new->start_addr = start_addr;
	new->end_addr = start_addr + len;

	/* Add space for the new entry */
	self->count++;
	self->handlers = realloc(self->handlers, sizeof(*self->handlers) * self->count);

	/* Update rounding to nearest power of two (for bisection in mem_find()) */
	if (self->count_pow2 < self->count) {
		self->count_pow2 <<= 1;
	};

	/* Move the new entry around to where the new entry belongs */
	self->handlers[self->count - 1] = new;
	for (i=0; i<self->count - 1; i++) {
		if (self->handlers[i]->start_addr > start_addr) {
			memmove(&self->handlers[i + 1], &self->handlers[i], sizeof(self->handlers) * (self->count - i - 1));
			self->handlers[i] = new;
			break;
		}
	}

	self->read_cache.handler = NULL;
	self->write_cache.handler = NULL;
	self->exec_cache.handler = NULL;

	return new;
}

__global int
mem_unregister(MemBus *self, struct mem_entry *entry)
{
	size_t i;

	if (!entry) return 0;

	for (i=0; i<self->count; i++) {
		if (self->handlers[i] == entry) {
			memmove(&self->handlers[i], &self->handlers[i+1], sizeof(self->handlers) * (self->count - i - 1));
			break;
		}
	}

	assert(i < self->count);    /* Entry must be registered in the array */

	self->count--;
	if (self->count_pow2 / 2 > self->count) {
		self->count_pow2 >>= 1;
	}

	if (self->count) {
		self->handlers = realloc(self->handlers, sizeof(self->handlers) * self->count);
	} else {
		free(self->handlers);
		self->handlers = NULL;
	}

	self->read_cache.handler = NULL;
	self->write_cache.handler = NULL;
	self->exec_cache.handler = NULL;

	mem_entry_free(entry);

	return 0;
}

__global int
mem_read(MemBus *self, uint64_t addr, void *data, size_t len)
{
	struct mem_entry *ptr;
	int ret;

	ptr = &self->read_cache;
	if (ptr->handler && addr >= ptr->start_addr && addr < ptr->end_addr) {
		return ptr->handler->read(ptr->handler->ctx, addr - ptr->start_addr, data, len);
	}

	/* Cache miss */
	ptr = mem_find(self, addr);

	if (!ptr) {
		log_warn("Unmapped read @ 0x%08"PRIx64" (bus %s)", addr, self->name);
		return EFAULT;
	}

	self->read_cache = *ptr;
	ret = ptr->handler->read(ptr->handler->ctx, addr - ptr->start_addr, data, len);

	return ret;
}

__global int
mem_write(MemBus *self, uint64_t addr, const void *data, size_t len)
{
	struct mem_entry *ptr;

	ptr = &self->write_cache;
	if (ptr->handler && addr >= ptr->start_addr && addr < ptr->end_addr) {
		return ptr->handler->write(ptr->handler->ctx, addr - ptr->start_addr, data, len);
	}

	/* Cache miss */
	ptr = mem_find(self, addr);

	if (!ptr) {
		log_warn("Unmapped write @ 0x%08"PRIx64" (bus %s)", addr, self->name);
		return EFAULT;
	}

	self->write_cache = *ptr;
	return ptr->handler->write(ptr->handler->ctx, addr - ptr->start_addr, data, len);
}

__global int
mem_execute(MemBus *self, uint64_t addr, void *instr, size_t len)
{
	struct mem_entry *ptr;
	int ret;

	/* Check cache first */
	ptr = &self->exec_cache;
	if (ptr->handler && addr >= ptr->start_addr && addr < ptr->end_addr) {
		return ptr->handler->execute(ptr->handler->ctx, addr - ptr->start_addr, instr, len);
	}

	/* Cache miss */
	ptr = mem_find(self, addr);

	if (!ptr) {
		log_warn("Unmapped exec @ 0x%08"PRIx64" (bus %s)", addr, self->name);
		return EFAULT;
	}

	self->exec_cache = *ptr;
	ret = ptr->handler->execute(ptr->handler->ctx, addr - ptr->start_addr, instr, len);

	return ret;
}

/* Static functions {{{ */
static void
mem_entry_free(struct mem_entry *ptr)
{
	free(ptr->handler);
	free(ptr);
}

static struct mem_entry*
mem_find(MemBus *self, uint64_t addr)
{
	size_t i;
	size_t increment;

	i = self->count_pow2 / 2;

	for (increment = self->count_pow2 / 2; increment > 0; increment /= 2) {
		if (i < self->count && addr >= self->handlers[i]->start_addr) {
			if (addr < self->handlers[i]->end_addr) {
				return self->handlers[i];
			} else {
				i += increment;
			}
		} else {
			i -= increment;
		}
	}

	if (i >= self->count || addr < self->handlers[i]->start_addr || addr >= self->handlers[i]->end_addr) return NULL;

	return self->handlers[i];
}
/* }}} */
