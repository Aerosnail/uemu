#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include "log.h"

/* Size of the path leading up to the project root, set by cmake at compile time */
#ifndef SOURCE_BASE_PATH_SIZE
#define SOURCE_BASE_PATH_SIZE 0
#endif

#define COLOR_DEFAULT "\033[;0m"

static enum loglevel _loglevel;
static const char* _colors[] = {
	[LOG_ERROR] = "\033[;31m",
	[LOG_WARN] = "\033[;33m",
	[LOG_INFO] = "\033[;32m",
	[LOG_DEBUG] = "\033[;34m",
};

__global void
log_set_level(enum loglevel loglevel)
{
	_loglevel = loglevel;
}

__global void
log_printf(enum loglevel loglevel, const char *funcname, const char *fmt, ...)
{
	va_list args;
	const char *tagcolor = _colors[loglevel];

	if (loglevel > _loglevel) return;

	printf("%s%16s " COLOR_DEFAULT "| ", tagcolor, funcname);
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\n");
}

__global void
log_hexdump(enum loglevel loglevel, const char *tag, int line, const void *v_data, size_t len)
{
	const uint8_t *data = v_data;
	const char *tagcolor = _colors[loglevel];
	size_t i;

	if (loglevel > _loglevel) return;

	fprintf(stderr, "[%s%s:%d" COLOR_DEFAULT "] ", tagcolor, tag + SOURCE_BASE_PATH_SIZE, line);

	for (i=0; i < len; i++) {
		if (!(i % 16)) fprintf(stderr, "\n%04zx\t", i);
		if (!(i % 8)) fprintf(stderr, " ");
		fprintf(stderr, "%02x ", data[i]);
	}
	fprintf(stderr, "\n");
}
