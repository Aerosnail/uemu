#pragma once

#include <stdlib.h>

#ifndef log_debug_hexdump
#ifdef NDEBUG
#define log_debug_hexdump(...)
#else
#define log_debug_hexdump(buf, len) log_hexdump(LOG_DEBUG, __func__, buf, len)
#endif
#endif

#ifndef log_error
#define log_error(...) log_printf(LOG_ERROR, __func__, __VA_ARGS__)
#endif
#ifndef log_warn
#define log_warn(...) log_printf(LOG_WARN, __func__, __VA_ARGS__)
#endif
#ifndef log_info
#define log_info(...) log_printf(LOG_INFO, __func__, __VA_ARGS__)
#endif
#ifndef log_debug
#ifdef NDEBUG
#define log_debug(...)
#else
#define log_debug(...) log_printf(LOG_DEBUG, __func__, __VA_ARGS__)
#endif
#endif


enum loglevel {
	LOG_NONE,
	LOG_ERROR,
	LOG_WARN,
	LOG_INFO,
	LOG_DEBUG
};

void log_set_level(enum loglevel loglevel);
void log_printf(enum loglevel loglevel, const char *funcname, const char *fmt, ...);
void log_hexdump(enum loglevel loglevel, const char *tag, int line, const void *data, size_t len);
