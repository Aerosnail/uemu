#pragma once

#include "memory.h"

typedef struct {
	MemBus *memory;
} CmdContext;

typedef int(*cmd_fcn_t)(int argc, const char* argv[], void* ctx);

/**
 * Register a new command
 *
 * @param cmd command (e.g. "map rom")
 * @param help optional string shown when executing "help $cmd"
 * @param execute function to execute on command invocation
 *
 * @return 0 on success, nonzero on failure
 */
int cmd_register(const char *cmd, const char *help, cmd_fcn_t execute, void *ctx);

/**
 * Register a command with the standard module prefix
 *
 */
int cmd_register_module(const char *name, const char *cmd, const char *help,
		cmd_fcn_t execute, void *ctx);

/**
 * Unregister a command
 *
 * @param cmd command to deregister
 *
 */
int cmd_unregister(const char *cmd);

/**
 * Unregister a command with the standard module prefix
 *
 */
int cmd_unregister_module(const char *name, const char *cmd);

/**
 * Execute a command
 *
 * @param argc number of arguments
 * @param argv arguments
 *
 * @return 0 on success, nonzero on failure
 */
int cmd_execute(int argc, const char *argv[]);


/**
 * Show the help string for a given command
 *
 * @param argc number of arguments
 * @param argv arguments
 * @param ctx  (ignored, used to conform to the cmd_fcn_t interface)
 *
 * @return 0 on success, nonzero on failure
 */
int cmd_show_help(int argc, const char *argv[], void *ctx);
