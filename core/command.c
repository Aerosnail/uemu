#define _XOPEN_SOURCE 500
#include <assert.h>
#include <errno.h>
#include <string.h>

#include "command.h"
#include "log/log.h"
#include "utils.h"

#define MAX_CMD_ARGCOUNT 32

enum cmd_type {
	CMD_TYPE_ATOMIC,
	CMD_TYPE_SUBCMD,
};

struct command {
	char *name;
	const char *help;

	enum cmd_type type;
	union {
		struct {
			cmd_fcn_t execute;
			void *ctx;
		} atomic;
		struct {
			struct command *commands;
		} subcmd;
	} cmd;

	struct command *next;
};

static struct command* cmd_register_internal(struct command **root, int argc,
		const char *argv[], const char *help, cmd_fcn_t execute, void *ctx);
static int cmd_unregister_internal(struct command **root, int argc,
		const char *argv[]);
static struct command *find_command_recursive(struct command *root, int argc,
		const char *argv[]);
static int clean_orphans(struct command **root);

static struct command *find_command(struct command *root, const char *name);
static int insert_command(struct command **root, struct command *leaf);
static int remove_command(struct command **root, struct command *cmd);

static struct {
	struct command *commands;
} _state;

__global int
cmd_register(const char *cmd, const char *help, cmd_fcn_t execute, void *ctx)
{
	const char *argv[MAX_CMD_ARGCOUNT];
	char *dup = strdup(cmd);

	int argc = to_argc_argv(argv, dup, MAX_CMD_ARGCOUNT);
	struct command *new_cmd;

	new_cmd = cmd_register_internal(&_state.commands, argc, argv, help, execute, ctx);
	free(dup);

	if (!new_cmd) return ENOMEM;
	return 0;
}

__global int
cmd_register_module(const char *name, const char *cmd, const char *help,
		cmd_fcn_t execute, void *ctx)
{
	const char *argv[MAX_CMD_ARGCOUNT + 2];
	char *dup = strdup(cmd);
	int argc;
	struct command *new_cmd;

	argv[0] = "module";
	argv[1] = name;
	argc = 2 + to_argc_argv(argv + 2, dup, MAX_CMD_ARGCOUNT);

	new_cmd = cmd_register_internal(&_state.commands, argc, argv, help, execute, ctx);
	free(dup);

	if (!new_cmd) return ENOMEM;
	return 0;
}

__global int
cmd_unregister(const char *cmd)
{
	const char *argv[MAX_CMD_ARGCOUNT];
	char *dup = strdup(cmd);
	int argc;
	int ret;

	argc = to_argc_argv(argv, dup, MAX_CMD_ARGCOUNT);

	ret = cmd_unregister_internal(&_state.commands, argc, argv);
	free(dup);

	return ret;
}

__global int
cmd_unregister_module(const char *name, const char *cmd)
{
	const char *argv[MAX_CMD_ARGCOUNT];
	char *dup = cmd ? strdup(cmd) : NULL;
	int argc;
	int ret = 0;
	struct command *ptr;

	if (!name) return EINVAL;

	argc = to_argc_argv(argv, dup, MAX_CMD_ARGCOUNT);

	/* Find "module" */
	ptr = find_command(_state.commands, "module");
	if (!ptr) goto failure;
	assert(ptr->type == CMD_TYPE_SUBCMD);

	/* Deregister subtree */
	if (!cmd) {
		ret = cmd_unregister_internal(&ptr->cmd.subcmd.commands, 1, &name);
	} else {
		ptr = find_command(ptr, name);
		if (!ptr) goto failure;
		ret = cmd_unregister_internal(&ptr->cmd.subcmd.commands, argc, argv);
	}
	free(dup);

	return ret;
failure:
	log_warn("Failed to delete commands for module %s", name);
	free(dup);
	return ENOKEY;
}

__global int
cmd_execute(int argc, const char *argv[])
{
	struct command *ptr;
	const char *subcmd;
	int i;

	if (!argc) return 0;
	if (!argv) return EINVAL;

	ptr = _state.commands;
	for (i=0; i<argc; i++) {
		subcmd = argv[i];

		ptr = find_command(ptr, subcmd);

		/* Command not found */
		if (!ptr) {
			log_error("\"%s\" not found", subcmd);
			return EINVAL;
		}

		switch (ptr->type) {
		case CMD_TYPE_ATOMIC:
			/* Command found */
			assert(ptr->cmd.atomic.execute);
			return ptr->cmd.atomic.execute(argc - i, argv + i, ptr->cmd.atomic.ctx);

		case CMD_TYPE_SUBCMD:
			/* Subcommand found */
			ptr = ptr->cmd.subcmd.commands;
			break;

		default:
			log_error("Invalid command type %d", ptr->type);
			return EINVAL;
		}
	}

	log_error("Unreachable code reached");
	return EINVAL;
}

__global int
cmd_show_help(int argc, const char *argv[], void *ctx)
{
	struct command *ptr;
	(void)ctx;

	if (argc < 2) {
		printf("Known commands:\n");
		for (ptr = _state.commands; ptr != NULL; ptr = ptr->next) {
			printf("%s\n", ptr->name);
		}
		return ENOKEY;
	}

	/* Traverse the tree to find the command */
	ptr = find_command_recursive(_state.commands, argc - 1, argv + 1);
	if (!ptr) {
		log_warn("Command not found");
		return ENOKEY;
	}

	switch (ptr->type) {
	case CMD_TYPE_SUBCMD:
		printf("%s is a prefix with the following subcommands:\n", ptr->name);
		for (ptr = ptr->cmd.subcmd.commands; ptr != NULL; ptr = ptr->next) {
			printf("%s\n", ptr->name);
		}
		printf("\n");
		break;

	case CMD_TYPE_ATOMIC:
		if (ptr->help) {
			printf("%s\n", ptr->help);
		} else {
			printf("No help available for %s", ptr->name);
		}
		break;

	default:
		log_warn("Unhandled command type %d", ptr->type);
		return EINVAL;
	}

	return 0;

}

/* Static functions {{{ */
static struct command*
cmd_register_internal(struct command **root, int argc, const char *argv[],
		const char *help, cmd_fcn_t execute, void *ctx)
{
	int i, ret;
	struct command *leaf, *ptr;
	const char *subcmd;

	/* Traverse tree of subcommands */
	for (i=0; i<argc - 1; i++) {
		subcmd = argv[i];

		ptr = find_command(*root, subcmd);

		if (!ptr) {
			/* Subtree empty: head insert and update root node */
			leaf = calloc(1, sizeof(*leaf));
			if (!leaf) return NULL;

			leaf->name = strdup(subcmd);
			leaf->type = CMD_TYPE_SUBCMD;
			leaf->next = NULL;
			leaf->cmd.subcmd.commands = NULL;

			ret = insert_command(root, leaf);
			assert(!ret);
		} else {
			/* Subtree not empty: use it */
			leaf = ptr;
		}

		root = &leaf->cmd.subcmd.commands;
	}

	/* Create new command */
	assert(root);

	leaf = calloc(1, sizeof(*leaf));
	if (!leaf) return NULL;

	leaf->name = strdup(argv[argc - 1]);
	leaf->help = help;
	leaf->type = CMD_TYPE_ATOMIC;
	leaf->next = NULL;
	leaf->cmd.atomic.execute = execute;
	leaf->cmd.atomic.ctx = ctx;
	ret = insert_command(root, leaf);
	assert(!ret);

	return leaf;
}

static int
cmd_unregister_internal(struct command **root, int argc, const char *argv[])
{
	struct command *ptr, **parent;
	int ret = 0;

	if (argc == 0) {
		ret |= remove_command(root, *root);
		goto cleanup;
	} else if (argc == 1) {
		ptr = find_command(*root, argv[0]);
		assert(ptr);
		ret |= remove_command(root, ptr);
		goto cleanup;
	}

	/* Find the root of the requested subtree */
	ptr = find_command_recursive(*root, argc - 1, argv);
	assert(ptr->type == CMD_TYPE_SUBCMD);
	parent = &ptr->cmd.subcmd.commands;

	ptr = find_command(*parent, argv[argc - 1]);

	/* Delete recursively */
	ret |= remove_command(parent, ptr);

cleanup:
	/* Clean up orphan nodes */
	ret |= clean_orphans(&_state.commands);

	return ret;
}


static struct command *
find_command_recursive(struct command *root, int argc, const char *argv[])
{
	struct command *ptr;
	int i;

	if (argc < 1) return root;

	ptr = root;
	for (i=0; i < argc - 1; i++) {
		ptr = find_command(ptr, argv[i]);
		if (!ptr) return NULL;
		if (ptr->type != CMD_TYPE_SUBCMD) return NULL;

		ptr = ptr->cmd.subcmd.commands;
	}

	if (!ptr) return NULL;
	ptr = find_command(ptr, argv[argc - 1]);

	return ptr;

}

static struct command*
find_command(struct command *root, const char *name)
{
	struct command *ptr;

	if (!name) return NULL;

	for (ptr = root; ptr != NULL; ptr = ptr->next) {
		if (!strcmp(ptr->name, name)) {
			return ptr;
		}
	}

	return NULL;
}

static int
insert_command(struct command **root, struct command *leaf)
{
	struct command *ptr;
	int cmp;

	assert(root);
	assert(leaf);

	if (!*root) {
		/* Subtree empty: head insert and update root node */
		*root = leaf;
		return 0;
	}

	for (ptr = *root; ptr->next != NULL; ptr = ptr->next) {
		cmp = strcmp(leaf->name, ptr->name);

		if (cmp > 0) {
			/* Leaf comes after ptr and before ptr->next */
			leaf->next = ptr->next;
			ptr->next = leaf;
			return 0;
		} else if (cmp == 0) {
			/* Command already in list */
			return EADDRINUSE;
		}
	}

	leaf->next = ptr->next;
	ptr->next = leaf;
	return 0;
}

static int
clean_orphans(struct command **root)
{
	struct command *tmp, *next;
	int ret = 0;

	/* Scan all commands in the list */
	for (tmp = *root; tmp != NULL; tmp = next) {
		next = tmp->next;

		/* Skip things that are not subcommands */
		if (tmp->type != CMD_TYPE_SUBCMD) continue;

		/* Recursively scan subtree */
		ret |= clean_orphans(&tmp->cmd.subcmd.commands);

		/* If tree is empty, unlink current root as well */
		if (!tmp->cmd.subcmd.commands) {
			log_debug("Subtree %s has no leaves, deleting...", tmp->name);
			ret |= remove_command(root, tmp);
		}
	}

	return ret;

}


static int
remove_command(struct command **root, struct command *cmd)
{
	struct command *tmp, *next;
	int ret = 0;

	assert(root);
	assert(cmd);
	assert(cmd->name);

	if (!*root) {
		return 0;
	}

	/* If it's a subcommand, recursively delete the subtree as well */
	if (cmd->type == CMD_TYPE_SUBCMD) {
		for (tmp = cmd->cmd.subcmd.commands; tmp != NULL; tmp = next) {
			next = tmp->next;
			ret |= remove_command(&cmd->cmd.subcmd.commands, tmp);
		}
	}

	/* If it's the root note, move the root */
	if (cmd == *root) {
		*root = cmd->next;

		free(cmd->name);
		free(cmd);
		return ret;
	}

	/* Else, unlink */
	for (tmp = *root; tmp->next != NULL; tmp = tmp->next) {
		if (tmp->next == cmd) {
			tmp->next = tmp->next->next;

			/* Free command */
			free(cmd->name);
			free(cmd);
			return ret;
		}
	}

	log_warn("Could not remove cmd: not found in given root");
	return ENODATA;
}
/* }}} */
