#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <log/log.h>
#include <utils.h>

#include "gpio.h"

/**
 * GPIOs are allocated in a contiguous array. When a connection between two
 * GPIOs is made, the two are connected to one another in a duobly linked list,
 * but still remain in the original array
 */

struct gpio {
	char *instance;         /* Owning instance name */
	char *name;             /* Pin name */
	enum gpio_type type;    /* Pin type (input/output/??) */
	enum gpio_pull pull;    /* Optional internal pull up/down resistor */
	int_fast8_t value;      /* Underlying pin value */

	void *ctx;
	int (*interrupt)(void *ctx, int value); /* Optional, called when the pin is
	                                           an input and the value of a
	                                           connected output changes */

	struct gpio *prev, *next;
};

static int gpio_free(struct gpio *gpio);
static struct gpio* gpio_alloc(const char *instance, const char *pin);
static int propagate_pin_change(struct gpio *group);
static int gpio_is_driving_strong(struct gpio *gpio);
static int gpio_is_driving_weak(struct gpio *gpio);

static struct {
	struct gpio **groups;
	size_t len;
} _state;

__global int
gpio_init(void)
{
	return 0;
}

__global struct gpio*
gpio_create_instance(const char *instance_name, const char *pin_name,
		 int (*on_value_changed)(void *ctx, int value), void *ctx)
{
	struct gpio *gpio;

	gpio = gpio_alloc(instance_name, pin_name);
	if (!gpio) {
		log_error("Failed to allocate pin %s:%s", instance_name, pin_name);
		return NULL;
	}

	/* Populate pin metadata */
	gpio->type = GPIO_TYPE_INPUT;
	gpio->pull = GPIO_PULL_NONE;
	gpio->value = 0;
	gpio->interrupt = on_value_changed;
	gpio->ctx = ctx;

	gpio->next = gpio->prev = gpio;

	/* Register as a pin in its own group */
	_state.len++;
	_state.groups = realloc(_state.groups, sizeof(*_state.groups) * (_state.len));
	_state.groups[_state.len - 1] = gpio;

	return gpio;
}

__global int
gpio_delete_instance(struct gpio *gpio)
{
	size_t i;

	if (!gpio) return 0;

	/* Unlink */
	gpio->next->prev = gpio->prev;
	gpio->prev->next = gpio->next;

	/* Free underlying GPIO */
	gpio_free(gpio);

	/* Remove from list */
	for (i = 0; i < _state.len; i++) {
		if (_state.groups[i] == gpio) {
			memmove(&_state.groups[i],
					&_state.groups[i + 1],
					sizeof(_state.groups[0]) * (_state.len - i - 1)
			);
			_state.len--;
			if (_state.len) {
				_state.groups = realloc(_state.groups, sizeof(*_state.groups) * (_state.len));
			} else {
				free(_state.groups);
				_state.groups = NULL;
			}
			return 0;
		}
	}

	log_error("GPIO not found in list");
	return ENOKEY;
}

__global int
gpio_write(struct gpio *gpio, int value)
{
	int ret = 0;

	assert(gpio);
	if (gpio->value == value) return 0;
	gpio->value = value;

	if (gpio_is_driving_strong(gpio)) {
		ret = propagate_pin_change(gpio);
	}

	return ret;
}


__global int
gpio_set_type(struct gpio *gpio, enum gpio_type type)
{
	assert(gpio);
	if (gpio->type == type) return 0;
	gpio->type = type;

	/* Recompute pin group output */
	return propagate_pin_change(gpio);
}

__global int
gpio_set_pull(GPIO *gpio, enum gpio_pull pull)
{
	assert(gpio);
	if (gpio->pull == pull) return 0;
	gpio->pull = pull;

	return propagate_pin_change(gpio);
}

__global int
gpio_read(struct gpio *gpio)
{
	struct gpio *ptr;
	struct gpio *output = NULL;
	struct gpio *pull = NULL;

	assert(gpio);

	/* Find output in group */
	ptr = gpio;
	do {
		if (gpio_is_driving_strong(ptr)) {
			output = ptr;
		} else if (gpio_is_driving_weak(ptr)) {
			pull = ptr;
		}

		ptr = ptr->next;
	} while (!output && ptr != gpio);


	if (!output && !pull) {
		log_warn("Pin %s:%s is floating", gpio->instance, gpio->name);
		return 0;
	}

	if (output) {
		log_debug("Pin %s:%s writer %s:%s", gpio->instance, gpio->name,
				output->instance, output->name);

		return output->value ? 1 : 0;
	} else {
		log_debug("Pin %s:%s puller %s:%s", gpio->instance, gpio->name,
				pull->instance, pull->name);

		return pull->pull == GPIO_PULL_UP ? 1 : 0;
	}
}

__global struct gpio*
gpio_find_by_name(const char *instance, const char *name)
{
	size_t i;

	for (i=0; i<_state.len; i++) {

		assert(_state.groups[i]->instance);
		assert(_state.groups[i]->name);

		if (!strcmp(_state.groups[i]->instance, instance) &&
		    !strcmp(_state.groups[i]->name, name)) {

			return _state.groups[i];
		}
	}

	log_warn("Pin %s:%s not found", instance, name);
	return NULL;
}


__global int
gpio_connect(struct gpio *gpio1, struct gpio *gpio2)
{
	const char *mod1, *name1, *mod2, *name2;
	struct gpio *tmp_prev;
	struct gpio *tmp_next;

	if (!gpio1) return EINVAL;
	if (!gpio2) return EINVAL;

	mod1 = gpio1->instance;
	mod2 = gpio2->instance;
	name1 = gpio1->name;
	name2 = gpio2->name;

	log_debug("%s:%s@%p <-> %s:%s@%p", mod1, name1, gpio1, mod2, name2, gpio2);

	/* Break gpio1 chain */
	tmp_prev = gpio1->prev;
	gpio1->prev->next = NULL;
	gpio1->prev = NULL;

	/* Attach gpio1 chain to gpio2 */
	tmp_next = gpio2->next;
	gpio2->next = gpio1;
	gpio1->prev = gpio2;

	/* Append rest of gpio2 chain to the end of gpio1 */
	tmp_prev->next = tmp_next;
	tmp_next->prev = tmp_prev;

	return 0;
}


__global int
gpio_disconnect(GPIO *gpio1, GPIO *gpio2)
{
	const char *mod1, *name1, *mod2, *name2;
	struct gpio *tmp;

	if (!gpio1) return EINVAL;
	if (!gpio2) return EINVAL;

	mod1 = gpio1->instance;
	mod2 = gpio2->instance;
	name1 = gpio1->name;
	name2 = gpio2->name;

	log_debug("%s:%s @ %p", mod1, name1, gpio1);
	log_debug("%s:%s @ %p", mod2, name2, gpio2);

	if (!gpio1) return ENODEV;
	if (!gpio2) return ENODEV;

	/* Ensure that gpio1 is connected to gpio2 */
	for (tmp = gpio1->next; tmp != gpio1; tmp = tmp->next) {
		if (tmp == gpio2) {
			log_debug("Unlinking %s:%s from %s:%s",
			          gpio1->instance, gpio1->name,
			          gpio2->instance, gpio2->name
			);

			/* Disconnect */
			gpio1->next->prev = gpio1->prev;
			gpio1->prev->next = gpio1->next;
			return 0;
		}
	}

	log_warn("%s:%s not connected to %s:%s",
	         gpio1->instance, gpio1->name,
	         gpio2->instance, gpio2->name
	);
	return 0;
}

/* Static functions {{{ */
static int
propagate_pin_change(struct gpio *gpio)
{
	struct gpio *output = NULL;
	struct gpio *pull = NULL;
	struct gpio *ptr;
	int_fast8_t value;

	/* If group is not connected to anything, there are no targets to propagate
	 * to anyway */
	if (!gpio->next) {
		assert(!gpio->prev);
		return 0;
	}

	/* Find output in group */
	ptr = gpio;
	do {
		if (gpio_is_driving_strong(ptr)) {
			output = ptr;
		} else if (gpio_is_driving_weak(ptr)) {
			pull = ptr;
		}

		ptr = ptr->next;
	} while (!output && ptr != gpio);

	if (output) {
		/* Strong driver */
		value = output->value;
	} else if (pull) {
		/* Weak driver */
		value = pull->pull == GPIO_PULL_UP;
	} else {
		/* Group is floating */
		return 0;
	}

	/* Call interrupt on every pin that wants to be notified */
	ptr = gpio;
	do {
		assert(ptr);
		if (ptr->type == GPIO_TYPE_INPUT && ptr->interrupt) {
			ptr->interrupt(ptr->ctx, value);
		}
		ptr = ptr->next;
	} while (ptr != gpio);

	return 0;
}

static struct gpio*
gpio_alloc(const char *instance, const char *pin)
{
	struct gpio *gpio = calloc(1, sizeof(*gpio));
	if (!gpio) return NULL;

	gpio->instance = strdup(instance);
	gpio->name = strdup(pin);

	return gpio;
}

static int
gpio_free(struct gpio *gpio)
{
	if (!gpio) return EINVAL;

	free(gpio->instance);
	free(gpio->name);
	free(gpio);
	return 0;
}

static int
gpio_is_driving_strong(struct gpio *gpio)
{
	switch (gpio->type) {
	case GPIO_TYPE_INPUT:
		return 0;
	case GPIO_TYPE_OUTPUT_PP:
		return 1;
	case GPIO_TYPE_OUTPUT_OD:
		return gpio->value == 0;
	}

	return 0;
}

static int
gpio_is_driving_weak(struct gpio *gpio)
{
	return gpio->pull != GPIO_PULL_NONE;
}
/* }}} */
