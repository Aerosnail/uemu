#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include <log/log.h>

#include "clock.h"
#include "utils.h"

struct clock {
	char *name;
	uint32_t freq;
	uint32_t cycle;
	int periph_count;

	int wait_count;
	uint32_t min_delta;
	pthread_mutex_t wait_count_mtx;
	pthread_cond_t wait_count_cond;
};

struct clock_periph {
	struct clock *parent;
	uint32_t cycle;
};

static int clock_free(struct clock *clock);

static struct {
	struct clock **clocks;
	size_t count;
} _state;

__global int
clock_mgr_init(void)
{
	_state.count = 0;
	_state.clocks = NULL;

	return 0;
}

__global int
clock_mgr_deinit(void)
{
	struct clock *clock;
	size_t i;

	for (i=0; i<_state.count; i++) {
		clock = _state.clocks[i];
		clock_free(clock);
	}

	free(_state.clocks);
	_state.count = 0;

	return 0;
}

__global struct clock*
clock_create(const char *name)
{
	struct clock *self;
	size_t i;

	/* Check that clock does not already exist */
	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.clocks[i]->name, name)) {
			return _state.clocks[i];
		}
	}

	self = calloc(1, sizeof(*self));
	if (!self) return NULL;

	_state.count++;
	_state.clocks = realloc(_state.clocks, _state.count * sizeof(*_state.clocks));
	_state.clocks[_state.count - 1] = self;

	self->name = strdup(name);

	self->cycle = 0;
	self->wait_count = 0;
	self->periph_count = 0;
	self->min_delta = -1U;

	pthread_cond_init(&self->wait_count_cond, NULL);
	pthread_mutex_init(&self->wait_count_mtx, NULL);

	return self;
}

__global int
clock_delete(const char *name)
{
	struct clock *clock;
	size_t i;

	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.clocks[i]->name, name)) {
			clock = _state.clocks[i];

			_state.count--;
			_state.clocks[i] = _state.clocks[_state.count];

			if (_state.count) {
				_state.clocks = realloc(_state.clocks, _state.count * sizeof(*_state.clocks));
			} else {
				free(_state.clocks);
				_state.clocks = NULL;
			}

			/* FIXME: this assert should really hold here, but for it to hold
			 * the peripheral clocks must be deinit'd before the main clock,
			 * which is not guaranteed to occur */
			//assert(!clock->periph_count);
			free(clock->name);
			free(clock);
			return 0;
		}
	}

	log_error("Failed to find clock '%s'", name);
	return ENOKEY;
}

__global struct clock*
clock_get(const char *name)
{
	size_t i;

	assert(name);

	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.clocks[i]->name, name)) {
			return _state.clocks[i];
		}
	}

	return clock_create(name);
}

__global int
clock_set_freq(Clock *clock, uint32_t freq)
{
	assert(clock);
	clock->freq = freq;

	return 0;
}

__global struct clock_periph*
clock_add_peripheral(struct clock *clock)
{
	struct clock_periph *ret;

	assert(clock);

	ret = calloc(1, sizeof(*ret));
	if (!ret) return NULL;

	ret->parent = clock;
	ret->cycle = clock->cycle;
	__atomic_add_fetch(&clock->periph_count, 1, __ATOMIC_SEQ_CST);

	return ret;
}

__global int
clock_del_peripheral(struct clock_periph *periph)
{
	assert(periph);

	__atomic_sub_fetch(&periph->parent->periph_count, 1, __ATOMIC_SEQ_CST);

	/* Unlock potentially waiting threads */
	pthread_mutex_lock(&periph->parent->wait_count_mtx);
	if (periph->parent->wait_count >= periph->parent->periph_count) {
		periph->parent->cycle += periph->parent->min_delta;
		pthread_cond_broadcast(&periph->parent->wait_count_cond);
	}
	pthread_mutex_unlock(&periph->parent->wait_count_mtx);

	free(periph);
	return 0;
}

__global int
clock_wait(struct clock_periph *self, uint32_t cycles)
{
	struct clock *parent = self->parent;

	if (!cycles) return 0;

	assert(self);
	assert(self->parent);
	assert(self->cycle <= parent->cycle);

	while (parent->cycle - self->cycle < cycles) {
		pthread_mutex_lock(&parent->wait_count_mtx);
		parent->wait_count++;

		if (parent->wait_count >= parent->periph_count) {
			/* We must produce cycles to avoid locking everything up */
			parent->cycle += MIN(cycles, parent->min_delta);
			parent->min_delta = -1U;
			pthread_cond_broadcast(&parent->wait_count_cond);
		} else {
			/* Some other peripheral has not yet used up the cycle budget: wait
			 * for everyone else to catch up */
			parent->min_delta = MIN(parent->min_delta, cycles);
			pthread_cond_wait(&parent->wait_count_cond, &parent->wait_count_mtx);
		}

		parent->wait_count--;
		pthread_mutex_unlock(&parent->wait_count_mtx);
	}

	self->cycle += cycles;
	return 0;
}

__global int
clock_force_unlock(struct clock_periph *self)
{
	assert(self);
	assert(self->parent);

	/* By forcing the cycle count to parent + 1, from the perspective of the
	 * thread waiting, the clock has just advanced by INT_MAX - 1 */
	pthread_mutex_lock(&self->parent->wait_count_mtx);
	self->cycle = self->parent->cycle + 1;
	pthread_cond_broadcast(&self->parent->wait_count_cond);
	pthread_mutex_unlock(&self->parent->wait_count_mtx);
}

static int
clock_free(struct clock *self)
{
	assert(self);
	self->cycle = -1;

	pthread_cond_broadcast(&self->wait_count_cond);
	pthread_cond_destroy(&self->wait_count_cond);
	pthread_mutex_destroy(&self->wait_count_mtx);

	free(self->name);
	free(self);

	return 0;
}
