#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <log/log.h>
#include <utils.h>

#include "interrupts.h"

#define MIN_IRQ_COUNT 256
#define IRQ_ERASED_MARKER -1

struct interrupt_mgr {
	char *name;
	int irq_count;

	int *prios;
	uint_fast8_t *masked;

	int *pending;
	int pending_count;
};

static int int_get_next_pending_internal(struct interrupt_mgr *self);
static int int_mgr_free(struct interrupt_mgr *manager);
static int set_irq_count(struct interrupt_mgr *self, int count);

static struct {
	struct interrupt_mgr **managers;
	size_t count;
} _state;

__global int
int_mgr_init(void)
{
	_state.managers = NULL;
	_state.count =  0;

	return 0;
}

__global int
int_mgr_deinit(void)
{
	struct interrupt_mgr *mgr;
	size_t i;
	int ret = 0;

	for (i=0; i<_state.count; i++) {
		mgr = _state.managers[i];

		ret |= int_mgr_free(mgr);
	}

	free(_state.managers);
	_state.count = 0;

	return ret;
}

__global struct interrupt_mgr*
int_mgr_create(const char *name)
{
	struct interrupt_mgr *self;

	self = calloc(1, sizeof(*self));
	if (!self) return NULL;

	_state.count++;
	_state.managers = realloc(_state.managers, _state.count * sizeof(*_state.managers));
	_state.managers[_state.count - 1] = self;

	self->name = strdup(name);

	self->irq_count = MIN_IRQ_COUNT;
	self->prios = calloc(self->irq_count, sizeof(*self->prios));
	self->pending = calloc(self->irq_count, sizeof(*self->pending));
	self->masked = calloc(self->irq_count, sizeof(*self->masked));

	if (!self->prios) return NULL;
	if (!self->pending) return NULL;
	if (!self->masked) return NULL;

	self->pending_count = 0;

	log_debug("Created interrupt mgr \"%s\"", name);

	return self;
}

__global int
int_mgr_delete(const char *name)
{
	struct interrupt_mgr *mgr;
	size_t i;

	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.managers[i]->name, name)) {
			mgr = _state.managers[i];

			_state.count--;
			_state.managers[i] = _state.managers[_state.count];

			if (_state.count) {
				_state.managers = realloc(_state.managers, _state.count * sizeof(*_state.managers));
			} else {
				free(_state.managers);
				_state.managers = NULL;
			}

			free(mgr->name);
			free(mgr);
			return 0;
		}
	}

	log_error("Failed to find interrupt space '%s'", name);
	return ENOKEY;

	return 0;
}

__global struct interrupt_mgr*
int_mgr_get(const char *name)
{
	size_t i;

	for (i=0; i<_state.count; i++) {
		if (!strcmp(_state.managers[i]->name, name)) {
			return _state.managers[i];
		}
	}

	return NULL;
}

__global int
int_set_pending(struct interrupt_mgr *self, int irq, int pending)
{
	int count;
	int i;
	int ret = 0;

	if (!self) return EINVAL;
	if (irq < 0) return EINVAL;
	if (irq >= self->irq_count) {
		/* Resize irq arrays to fit */
		ret = set_irq_count(self, irq);

		if (ret) return ret;
	}

	count = self->pending_count;

	for (i = 0; i < count; i++) {
		if (self->pending[i] == irq) {
			if (pending) {
				/* Was pending, still is pending */
				return 0;
			} else {
				/* Was pending, no longer pending */
				self->pending[i] = IRQ_ERASED_MARKER;
				return 0;
			}
		}
	}

	if (pending) {
		/* Was not pending, is pending */
		self->pending[count] = irq;
		__atomic_add_fetch(&self->pending_count, 1, __ATOMIC_SEQ_CST);
	}

	return 0;
}

__global int
int_get_pending(struct interrupt_mgr *self, int irq)
{
	int i;
	int count;

	if (!self) return 0;
	if (irq < 0 || irq >= self->irq_count) return 0;

	count = self->pending_count;
	for (i = 0; i < count; i++) {
		if (self->pending[i] == irq) return 1;
	}

	return 0;
}

__global int
int_get_next_pending(struct interrupt_mgr *self)
{
	if (__builtin_expect(self->pending_count <= 0, 1)) return -1;

	return int_get_next_pending_internal(self);
}

__global int
int_set_priority(struct interrupt_mgr *self, int irq, int prio)
{
	int ret;

	if (!self) return EINVAL;
	if (irq < 0) return EINVAL;
	if (irq >= self->irq_count) {
		/* Resize irq arrays to fit */
		ret = set_irq_count(self, irq);

		if (ret) return ret;
	}

	self->prios[irq] = prio;

	return 0;
}

__global int
int_get_priority(struct interrupt_mgr *self, int irq)
{
	if (irq < 0 || irq >= self->irq_count) return INT_MAX;
	return self->prios[irq];
}

__global int
int_set_masked(struct interrupt_mgr *self, int irq, int masked)
{
	int was_masked;
	int ret;

	if (!self) return EINVAL;
	if (irq < 0) return EINVAL;
	if (irq >= self->irq_count) {
		/* Resize irq arrays to fit */
		ret = set_irq_count(self, irq);

		if (ret) return ret;
	}

	was_masked = self->masked[irq];

	self->masked[irq] = masked;

	/* Update number of pending interrupts */
	if (!masked && was_masked && int_get_pending(self, irq)) {
		__atomic_add_fetch(&self->pending_count, 1, __ATOMIC_SEQ_CST);
	}

	return 0;
}

__global int
int_get_masked(struct interrupt_mgr *self, int irq)
{
	if (irq < 0 || irq >= self->irq_count) return 0;
	return self->masked[irq];
}

__attribute__((noinline))
static int
int_get_next_pending_internal(struct interrupt_mgr *self)
{
	int i;
	int idx, irq, next_irq;
	int prio;
	int count;

	count = __atomic_fetch_sub(&self->pending_count, 1, __ATOMIC_SEQ_CST);

	/* Search for the next pending, unmasked interrupt */
	irq = IRQ_ERASED_MARKER - 1;
	prio = INT_MAX;
	for (i = 0; i < count; i++) {
		next_irq = self->pending[i];

		if (next_irq > IRQ_ERASED_MARKER && !self->masked[next_irq] && self->prios[next_irq] < prio) {
			idx = i;
			irq = next_irq;
			prio = self->prios[next_irq];
		}
	}

	if (irq >= IRQ_ERASED_MARKER) {
		self->pending[idx] = self->pending[count - 1];
	}

	return irq;
}

static int
set_irq_count(struct interrupt_mgr *self, int count)
{
	void *tmp;

	tmp = realloc(self->prios, count * sizeof(*self->prios));
	if (!tmp) return ENOMEM;
	self->prios = tmp;

	tmp = realloc(self->pending, count * sizeof(*self->pending));
	if (!tmp) return ENOMEM;
	self->pending = tmp;

	tmp = realloc(self->masked, count * sizeof(*self->masked));
	if (!tmp) return ENOMEM;
	self->masked = tmp;

	self->irq_count = count;

	return 0;
}

static int
int_mgr_free(struct interrupt_mgr *self)
{
	if (!self) return EINVAL;

	free(self->prios);
	free(self->pending);
	free(self->masked);
	free(self->name);
	free(self);

	return 0;
}
