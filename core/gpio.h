#pragma once

#include <stdint.h>

enum gpio_type {
	GPIO_TYPE_INPUT,
	GPIO_TYPE_OUTPUT_PP,
	GPIO_TYPE_OUTPUT_OD,
};

enum gpio_pull {
	GPIO_PULL_NONE,
	GPIO_PULL_UP,
	GPIO_PULL_DOWN
};

typedef struct gpio GPIO;

/**
 * Initialize GPIO backend
 *
 * @return 0 on success, nonzero on failure
 */
int gpio_init(void);

/**
 * Create a new GPIO pin
 *
 * @param instance_name    name of the module creating this pin
 * @param pin_name         name of the pin
 * @param type             initial pin type
 * @param value            initial pin value
 * @param on_value_changed handler to call when the pin state changes (optional)
 * @param ctx              pointer to pass to the on_value_changed function
 *
 * @return NULL on error, else a handle to a GPIO object
 */
GPIO *gpio_create_instance(const char *instance_name, const char *pin_name,
		int (*on_value_changed)(void *ctx, int), void *ctx);

/**
 * Delete a GPIO object
 *
 * @param gpio GPIO to destroy
 *
 * @return 0 on success, nonzero on failure
 */
int gpio_delete_instance(GPIO *gpio);

/**
 * Change the type of a GPIO object
 *
 * @param gpio GPIO to affect
 * @param type new type to assign
 *
 * @return 0 on success, nonzero on failure
 */
int gpio_set_type(GPIO *gpio, enum gpio_type type);

/**
 * Change the pull up/down resistor status for a pin
 *
 * @param gpio GPIO to affect
 * @parem pull pull resistor type
 *
 * @return 0 on success, nonzero on failure
 */
int gpio_set_pull(GPIO *gpio, enum gpio_pull pull);

/**
 * Set the output of a GPIO object. If the pin is an input, store the value for
 * later but do not output it on connected pins.
 *
 * @param gpio  GPIO to affect
 * @param value new value
 *
 * @return 0 on success, nonzero on failure.
 */
int gpio_write(GPIO *gpio, int value);

/**
 * Read the input value from a GPIO
 *
 * @param gpio gpio to read
 *
 * @return 0/1 depending on the value of connected pins
 *         1 if no output pin is connected to the given pin (aka floating)
 */
int gpio_read(GPIO *gpio);

/**
 * Find a registered gpio pin from its name and the name of the module that
 * created it
 *
 * @param mod_name name of the module the pin belongs to
 * @param pin_name name of the pin
 *
 * @return handle to the pin (NULL on failure)
 */
GPIO* gpio_find_by_name(const char *mod_name, const char *pin_name);

/**
 * Connect two GPIO pins
 *
 * @param gpio1 gpio to connect (and detach from previous connections if any)
 * @param gpio2 gpio to connect to
 *
 * @return 0 on success, errno on failure
 */
int gpio_connect(GPIO *gpio1, GPIO *gpio2);

/**
 * Disconnect two GPIO pins
 *
 * @param gpio1 gpio to disconnect
 * @param gpio2 gpio to disconnect from
 *
 * @return 0 on success, errno on failure
 */
int gpio_disconnect(GPIO *gpio1, GPIO *gpio2);

