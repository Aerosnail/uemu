#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef VERSION
#define VERSION "(unknown version)"
#endif

/* setenv(), unsetenv() and strcasecmp() for Windows */
#ifdef _WIN32
#define setenv(env, val, overwrite)  _putenv_s(env, val)
#define unsetenv(env)                _putenv_s(env, "=")
#define strcasecmp _stricmp
#define mkdir _mkdir
#endif

/* Portable unroll pragma, for some reason clang defines __GNUC__ but uses the
 * non-GCC unroll pragma format */
#define DO_PRAGMA(x) _Pragma(#x)
#if defined(__clang__)
#define PRAGMA_UNROLL(x) DO_PRAGMA(unroll x)
#elif defined(__GNUC__)
#define PRAGMA_UNROLL(x) DO_PRAGMA(GCC unroll x)
#else
#define PRAGMA_UNROLL(x) DO_PRAGMA(unroll x)
#endif

/* Portable struct packed attribute */
#ifdef __GNUC__
#define PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#elif defined(_MSC_VER)
#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#endif

/* Portable always_inline */
#ifdef __GNUC__
#define FORCE_INLINE __attribute__((always_inline))
#elif defined(_MSC_VER)
#define FORCE_INLINE __forceinline
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define LEN(x) (sizeof(x)/sizeof(*(x)))
#ifndef sgn
#define sgn(x) ((x) < 0 ? -1 : 1)
#endif
#ifndef M_PI
#define M_PI 3.1415926536f
#endif

#define REG_DEF(__name__, __mask__, __shift__)  \
    static inline uint32_t \
    __name__ ## _get (uint32_t reg) \
    { \
        return ((reg) >> ( __shift__ )) & ( __mask__ ); \
    } \
    static inline void \
    __name__ ## _set (uint32_t *reg, uint32_t val) \
    { \
        *(reg) = (*(reg) & ~(( __mask__ ) << ( __shift__ ))) \
             | (((val) & ( __mask__ )) << ( __shift__ )); \
    } \
    static inline void \
    __name__ ## _clear (uint32_t *reg, uint32_t val) \
    { \
        *(reg) = *(reg) & ~(((val) & ( __mask__ )) << ( __shift__ )); \
    }


int to_argc_argv(const char *dst[], char *src, size_t max_argc);
int strbegins(const char *haystack, const char *needle);
