#include <errno.h>
#include <string.h>

#include "node.h"

enum property {
	PROP_COMPATIBLE = 0,
	PROP_ADDRESS_CELLS,
	PROP_SIZE_CELLS,
	PROP_REG,
	PROP_PHANDLE,

	PROP_COUNT
};

static const char *_known_props[] = {
	[PROP_COMPATIBLE] = "compatible",
	[PROP_ADDRESS_CELLS] = "#address-cells",
	[PROP_SIZE_CELLS] = "#size-cells",
	[PROP_REG] = "reg",
	[PROP_PHANDLE] = "phandle",
};

__global int
dtb_node_init(struct dtb_node *node, const struct dtb_node *parent)
{
	if (!node) return EINVAL;

	memset(node, 0, sizeof(*node));

	if (parent) {
		node->address_cells = parent->address_cells;
		node->size_cells = parent->size_cells;
	} else {
		node->address_cells = 2;
		node->size_cells = 1;
	}

	return 0;
}

__global int
dtb_node_deinit(struct dtb_node *node)
{
	if (!node) return EINVAL;

	free(node->reg);
	free(node->props);
	free(node->compatible);
	node->props = NULL;
	node->props_count = 0;

	return 0;
}

__global int
dtb_node_add_property(struct dtb_node *dst, const struct dtb_property *src)
{
	enum property prop;
	size_t i, j, count;
	uint32_t tmp;

	const char *name = src->name;
	size_t len = src->len;
	const char *data = src->data;

	/* Find property index by name */
	for (prop = 0; prop < PROP_COUNT; prop++) {
		if (!strcmp(name, _known_props[prop])) {
			break;
		}
	}

	/* Parse property */
	switch (prop) {
	case PROP_COMPATIBLE:
		count = 0;
		for (i = 0; i < len; i += strlen(data) + 1) {
			count++;
		}

		dst->compatible = calloc(count + 1, sizeof(*dst->compatible));
		dst->compatible[count] = NULL;

		for (i = 0; i < count; i++) {
			dst->compatible[i] = data;
			data += strlen(data) + 1;
		}
		break;

	case PROP_PHANDLE:
		if (len != sizeof(uint32_t)) return EINVAL;
		dtb_property_get_u32(&dst->phandle, src, 0, 1);
		break;

	case PROP_ADDRESS_CELLS:
		if (len != sizeof(uint32_t)) return EINVAL;
		dtb_property_get_u32(&dst->address_cells, src, 0, 1);
		break;

	case PROP_SIZE_CELLS:
		if (len != sizeof(uint32_t)) return EINVAL;
		dtb_property_get_u32(&dst->size_cells, src, 0, 1);
		break;

	case PROP_REG:
		if (!dst->address_cells || !dst->size_cells) return EINVAL;
		if (len % (sizeof(uint32_t) * (dst->address_cells + dst->size_cells))) return EINVAL;

		count = len / (sizeof(uint32_t) * (dst->address_cells + dst->size_cells));
		dst->reg_count = count;
		dst->reg = calloc(count, sizeof(*dst->reg));

		for (i = 0; i < count; i++) {
			/* Parse address cells */
			for (j = 0; j < dst->address_cells; j++) {
				dtb_property_get_u32(&tmp,
				                     src,
				                     i * (dst->address_cells + dst->size_cells) + j,
				                     1);
				dst->reg[i].address = (dst->reg[i].address << 32) + tmp;
			}

			/* Parse size cells */
			for (j = 0; j < dst->size_cells; j++) {
				dtb_property_get_u32(&tmp,
				                     src,
				                     i * (dst->address_cells + dst->size_cells) + dst->address_cells + j,
				                     1);
				dst->reg[i].size = (dst->reg[i].address << 32) + tmp;
			}
		}

		break;

	default:
		dst->props_count++;
		dst->props = realloc(dst->props, sizeof(*dst->props) * dst->props_count);
		dst->props[dst->props_count - 1].name = name;
		dst->props[dst->props_count - 1].len = len;
		dst->props[dst->props_count - 1].data = data;
		break;
	}

	return 0;
}

__global const struct dtb_property*
dtb_node_find_property(const struct dtb_node *node, const char *name)
{
	size_t i;

	if (!node) return NULL;

	for (i = 0; i < node->props_count; i++) {
		if (!strcmp(node->props[i].name, name)) {
			return &node->props[i];
		}
	}

	return NULL;
}
