#include <errno.h>

#include "property.h"

static void dtb_to_host(void *v_dst, const void *v_src, size_t offset, size_t len);

__global int
dtb_property_init(struct dtb_property *dst, const char *name, size_t len, const void *data)
{
	if (!dst) return EINVAL;

	dst->name = name;
	dst->len = len;
	dst->data = data;

	return 0;
}

__global int
dtb_property_get_u32(uint32_t *dst, const struct dtb_property *src, size_t offset, size_t count)
{
	if (!dst) return EINVAL;
	if (!src) return EINVAL;

	dtb_to_host(dst, src->data, offset, count);

	return 0;
}

static void
dtb_to_host(void *v_dst, const void *v_src, size_t offset, size_t len)
{
	uint32_t *dst = (uint32_t*)v_dst;
	uint32_t *src = (uint32_t*)v_src + offset;
	size_t i;

	for (i=0; i<len; i++) {
		dst[i] = (src[i] & 0xFF) << 24
		       | (src[i] & 0xFF << 8) << 8
		       | (src[i] & 0xFF << 16) >> 8
		       | src[i] >> 24;
	}
}
