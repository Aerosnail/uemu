#pragma once

#include <stdlib.h>
#include <stdint.h>

struct dtb_property {
	const char *name;   /* Property name */
	size_t len;         /* Propetry size (bytes) */
	const void *data;   /* Propetty data */
};

int dtb_property_init(struct dtb_property *dst, const char *name, size_t len, const void *data);

int dtb_property_get_u32(uint32_t *dst, const struct dtb_property *src, size_t offset, size_t count);
