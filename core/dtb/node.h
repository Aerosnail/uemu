#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "property.h"

struct dtb_reg {
	uint64_t address;
	uint64_t size;
};

struct dtb_node {
	/* Standard properties */
	const char *name;
	const char **compatible;
	uint32_t phandle;
	uint32_t address_cells;
	uint32_t size_cells;

	struct dtb_reg *reg;
	size_t reg_count;

	/* Free-form properties */
	struct dtb_property *props;
	size_t props_count;
};

int dtb_node_init(struct dtb_node *node, const struct dtb_node *parent);
int dtb_node_deinit(struct dtb_node *node);

int dtb_node_add_property(struct dtb_node *dst, const struct dtb_property *prop);

const struct dtb_property* dtb_node_find_property(const struct dtb_node *node, const char *name);
