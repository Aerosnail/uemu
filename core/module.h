#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "memory.h"
#include "dtb/node.h"

/* ------- From core to module ------- */
typedef const struct dtb_node* (*find_phandle_t)(void*, uint32_t);

typedef struct {
	MemBus *bus;        /* Handle to use when calling the mem_*() API */
	const char *name;           /* Module name */
	const char *compatible;     /* Compatibility string selected for this instance */

	struct dtb_reg *reg;
	size_t reg_count;

	void *ctx;
} ModuleSpecs;

/* ------- From module to core ------- */
typedef struct {
	const char *name;           /* Module name */
	const char *author;         /* Module author (optional) */
	int major, minor, patch;    /* Module version number, major.minor.patch */

	const char **compatible;    /* List of DTS device names that can be emulated by this module */
} ModuleInfo;
