#pragma once

#include <stdint.h>

typedef struct clock Clock;
typedef struct clock_periph ClockPeriph;

int clock_mgr_init(void);
int clock_mgr_deinit(void);

/**
 * Create a new clock generator with the given name
 */
Clock* clock_create(const char *name);
int clock_delete(const char *name);
Clock* clock_get(const char *name);

int clock_set_freq(Clock *clock, uint32_t freq);

ClockPeriph* clock_add_peripheral(Clock *clock);
int clock_del_peripheral(ClockPeriph *periph);

int clock_wait(struct clock_periph *self, uint32_t cycles);
int clock_force_unlock(struct clock_periph *self);
