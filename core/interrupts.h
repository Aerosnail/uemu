#pragma once

typedef struct interrupt_mgr InterruptManager;

/**
 * Initialize interrupt manager backend
 */
int int_mgr_init(void);

/**
 * Initialize interrupt manager backend
 */
int int_mgr_deinit(void);

/**
 * Create a new interrupt manager
 *
 * @param name  manager name, used for indexing with int_mgr_get
 *
 * @return pointer to the new manager on success, NULL on failure
 */
InterruptManager* int_mgr_create(const char *name);
int int_mgr_delete(const char *name);

/**
 * Get interrupt manager by name
 *
 * @param name  interrupt manager name
 *
 * @return pointer to interrupt manager on success, NULL on failure
 */
InterruptManager* int_mgr_get(const char *name);

/**
 * Set pending status for a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to affect
 * @param pending   new pending status (0 = not pending, 1 = pending)
 *
 * @return 0 on success, errno on failure
 */
int int_set_pending(InterruptManager *mgr, int irq, int pending);

/**
 * Get pending status for a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to check
 *
 * @return nonzero if pending, 0 if not pending
 */
int int_get_pending(InterruptManager *mgr, int irq);

/**
 * Get the number of the next IRQ to service
 *
 * @param mgr       interrupt manager
 *
 * @return pending IRQ number, < 0 if none pending
 * @note the IRQ returned by this function is automatically marked as not
 *       pending before returning
 */
int int_get_next_pending(InterruptManager *mgr);

/**
 * Set the priority of a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to affect
 * @param pending   new priority (lower number = higher prio)
 *
 * @return 0 on success, errno on failure
 */
int int_set_priority(InterruptManager *mgr, int irq, int prio);

/**
 * Get the priority of a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to check
 *
 * @return interrupt priority
 */
int int_get_priority(InterruptManager *mgr, int irq);

/**
 * Set masekd status for a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to affect
 * @param masekd    new masked status (0 = unmasked, 1 = masked)
 *
 * @return 0 on success, errno on failure
 */
int int_set_masked(InterruptManager *mgr, int irq, int masked);

/**
 * Get masked status for a given interrupt
 *
 * @param mgr       interrupt manager
 * @param irq       IRQ to check
 *
 * @return nonzero if masked, 0 if not masked
 */
int int_get_masked(InterruptManager *mgr, int irq);
