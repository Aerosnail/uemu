#include "utils.h"

int
to_argc_argv(const char *dst[], char *src, size_t max_argc)
{
	size_t argc = 0;
	size_t i;

	if (!src) return 0;
	if (*src == '\0') return 0;

	dst[argc++] = src;

	for (i=0; src[i] != '\0' && argc < max_argc; i++) {
		switch (src[i]) {
		case ' ':
			src[i] = '\0';
			for (i++; src[i] == ' '; i++)
				;
			dst[argc++] = src + i;
			break;

		case '"':
			/* TODO quoted strings are hard smh */
			break;

		default:
			break;
		}
	}

	if (*dst[argc - 1] == '\0') argc--;


	return argc;
}

__global int
strbegins(const char *haystack, const char *needle)
{
	while (*needle) {
		if (*needle != *haystack) return 0;
		needle++;
		haystack++;
	}

	return 1;
}
