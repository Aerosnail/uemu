#pragma once

#include <stdint.h>
#include <stdlib.h>

struct hydra_buf;
struct hydra_buf_consumer;

/**
 * @brief Initialize a buffer with one producer and multiple consumers
 *
 * @param elem_size     size of each element in the buffer
 * @param elem_count    number of elements in the buffer
 *
 * @return hydra buffer handle on success, NULL on failure
 */
struct hydra_buf* hydra_buf_init(size_t elem_size, size_t elem_count);

/**
 * @brief Initialize a buffer with one producer and multiple consumers
 *
 * @param handle    handle to an existing buffer
 *
 * @return 0 on success, errno on failure
 */
int hydra_buf_deinit(struct hydra_buf *handle);

/**
 * @brief Add a consumer to the buffer
 *
 * @param handle    handle to an existing buffer
 *
 * @return handle to consumer on success, NULL on failure
 */
struct hydra_buf_consumer* hydra_buf_add_consumer(struct hydra_buf *handle);

/**
 * @brief Delete a consumer
 *
 * @param consumer  consumer to delete
 *
 * @return 0 on success, errno on failure
 */
int hydra_buf_del_consumer(struct hydra_buf_consumer* consumer);

/**
 * @brief Push an elemeent to the ring buffer
 *
 * @param handle    handle to buffer
 * @param elem      element to push
 *
 * @return 0 on success, errno on failure
 */
int hydra_buf_push(struct hydra_buf *handle, const void *elem);

/**
 * @brief Indicate to all workers that the buffer will no longer produce items
 *
 * @param handle    handle to buffer
 *
 * @return 0 on success, errno on failure
 */
int hydra_buf_push_eos(struct hydra_buf *handle);

/**
 * @brief Pop an elemeent from the ring buffer
 *
 * @param handle    handle to consumer object
 * @param dst       pointer to buffer to write element to
 *
 * @return 0 on success, EINTR if the ring buffer will no longer produce items,
 * errno on failure
 */
int hydra_buf_pop(struct hydra_buf_consumer *handle, void *dst);

/**
 * @brief Pop an elemeent from the ring buffer
 *
 * @param handle    handle to consumer object
 * @param dst       pointer to buffer to write element to
 *
 * @return 0 on success, EINTR if the ring buffer will no longer produce items,
 * EAGAIN if no items are currently available, errno on failure
 */
int hydra_buf_pop_nonblocking(struct hydra_buf_consumer *handle, void *dst);
