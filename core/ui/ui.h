#pragma once

#include <stdint.h>

enum keyboard_event {
	KBD_EVT_KEYDOWN,
	KBD_EVT_KEYUP,
};

typedef struct ui_glcontext UIGLContext;
typedef struct ui_event_listener UIEvtListener;

/**
 * @brief Initialize UI subsystem
 */
int ui_init(void);

/**
 * @brief Deinitailize UI subsystem
 */
int ui_deinit(void);

int ui_get_window_size(int *width, int *height);

UIGLContext* ui_opengl_create_context(void);
int ui_opengl_delete_context(UIGLContext *ctx);
int ui_opengl_swap_buffers(UIGLContext *ctx);
void* ui_opengl_get_proc_address(const char *name);


UIEvtListener *ui_evt_create_listener(void);
int ui_evt_delete_listener(UIEvtListener *listener);
int ui_evt_poll_keyboard(UIEvtListener *ctx, enum keyboard_event *type, uint32_t *scancode);
int ui_evt_wait_keyboard(UIEvtListener *ctx, enum keyboard_event *type, uint32_t *scancode);
