#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <SDL2/SDL.h>
#include <stdlib.h>

#include <log/log.h>
#include "ui.h"
#include "hydrabuffer.h"

#define WINDOW_NAME "uemu"
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define EVT_BUF_DEPTH 64

struct ui_glcontext {
	SDL_GLContext glContext;
};

struct ui_event_listener {
	struct hydra_buf_consumer *consumer;
};

static struct {
	SDL_Window *win;
	int win_refcount;

	struct hydra_buf *evt_buf;
	int evt_buf_refcount;

	pthread_t tid;
} _state;

static int sdl_init(const char *name);
static int sdl_deinit(void);
static void* sdl_event_producer(void *args);

__global int
ui_init(void)
{
	_state.win_refcount = 0;
	_state.evt_buf_refcount = 0;

	return 0;
}

__global int
ui_deinit(void)
{
	/* Notify listeners that the UI is shutting down (cleanup will be performed
	 * automatically once resources are released) */
	if (_state.evt_buf) {
		hydra_buf_push_eos(_state.evt_buf);
	}

	return 0;
}

__global int
ui_get_window_size(int *width, int *height)
{
	if (!width || !height) return EINVAL;
	if (!_state.win) {
		*width = 0;
		*height = 0;
		return 0;
	}

	SDL_GetWindowSize(_state.win, width, height);
	return 0;
}

__global UIGLContext*
ui_opengl_create_context(void)
{
	UIGLContext *ctx;
	int ret;

	/*  Create window if there isn't one yet */
	if (!_state.win) {
		ret = sdl_init(WINDOW_NAME);
		if (ret) return NULL;
	}

	ctx = calloc(1, sizeof(*ctx));
	if (!ctx) return NULL;

	/* Create new context */
	ctx->glContext = SDL_GL_CreateContext(_state.win);
	if (!ctx->glContext) {
		log_error("Failed to create OpenGL context: %s", SDL_GetError());
		free(ctx);
		return NULL;
	}

	if (ctx) __atomic_add_fetch(&_state.win_refcount, 1, __ATOMIC_SEQ_CST);
	return ctx;
}

__global int
ui_opengl_delete_context(UIGLContext *ctx)
{
	int ret = 0;

	if (!ctx) return EINVAL;

	SDL_GL_DeleteContext(ctx->glContext);
	free(ctx);

	if (__atomic_sub_fetch(&_state.win_refcount, 1, __ATOMIC_SEQ_CST) <= 0) {
		ret = sdl_deinit();
	}

	return ret;
}

__global int
ui_opengl_swap_buffers(UIGLContext *ctx)
{
	(void)ctx;

	SDL_GL_SwapWindow(_state.win);

	return 0;
}

__global void*
ui_opengl_get_proc_address(const char *name)
{
	return SDL_GL_GetProcAddress(name);
}

__global UIEvtListener*
ui_evt_create_listener(void)
{
	UIEvtListener *listener;

	listener = calloc(1, sizeof(*listener));
	if (!listener) return NULL;

	/* Create event buffer if there is none */
	if (!_state.evt_buf) {
		_state.evt_buf = hydra_buf_init(sizeof(SDL_Event), EVT_BUF_DEPTH);
	}

	/* Add listener */
	listener->consumer = hydra_buf_add_consumer(_state.evt_buf);
	if (!listener->consumer) {
		free(listener);
		return NULL;
	}

	__atomic_add_fetch(&_state.evt_buf_refcount, 1, __ATOMIC_SEQ_CST);

	return listener;
}

__global int
ui_evt_delete_listener(UIEvtListener *listener)
{
	int ret;
	if (!listener) return EINVAL;

	ret = hydra_buf_del_consumer(listener->consumer);
	free(listener);

	if (__atomic_sub_fetch(&_state.evt_buf_refcount, 1, __ATOMIC_SEQ_CST) <= 0) {
		/* Free event parser */
		hydra_buf_deinit(_state.evt_buf);
		_state.evt_buf = NULL;
	}
	return ret;
}

__global int
ui_evt_poll_keyboard(UIEvtListener *ctx, enum keyboard_event *type, uint32_t *scancode)
{
	SDL_Event evt;
	int ret;

	if (!ctx) return EINVAL;
	if (!ctx->consumer) return EINVAL;

	do {
		ret = hydra_buf_pop_nonblocking(ctx->consumer, &evt);
		if (ret) return ret;

		switch (evt.type) {
		case SDL_QUIT:
			return EINTR;
		case SDL_KEYDOWN:
			if (type) *type = KBD_EVT_KEYDOWN;
			if (scancode) *scancode = evt.key.keysym.sym;
			break;
		case SDL_KEYUP:
			if (type) *type = KBD_EVT_KEYUP;
			if (scancode) *scancode = evt.key.keysym.sym;
			break;
		default:
			ret = 1;
			break;
		}
	} while (ret);

	return 0;

}

__global int
ui_evt_wait_keyboard(UIEvtListener *ctx, enum keyboard_event *type, uint32_t *scancode)
{
	SDL_Event evt;
	int ret;

	if (!ctx) return EINVAL;
	if (!ctx->consumer) return EINVAL;

	do {
		ret = hydra_buf_pop(ctx->consumer, &evt);
		if (ret) return ret;

		switch (evt.type) {
		case SDL_QUIT:
			return EINTR;
		case SDL_KEYDOWN:
			*type = KBD_EVT_KEYDOWN;
			*scancode = evt.key.keysym.scancode;
			break;
		case SDL_KEYUP:
			*type = KBD_EVT_KEYUP;
			*scancode = evt.key.keysym.scancode;
			break;
		default:
			ret = 1;
			break;
		}
	} while (ret);

	return 0;
}

/* Static functions {{{ */
static int
sdl_init(const char *name)
{
	/* Initialize SDL2 */
	SDL_SetHintWithPriority(SDL_HINT_RENDER_VSYNC, "1", SDL_HINT_OVERRIDE);
	SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "0");
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS)) {
		log_error("SDL failed to initialize: %s\n", SDL_GetError());
		return EIO;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetSwapInterval(1);

	/* Create new SDL window */
	_state.win = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	                              WINDOW_WIDTH, WINDOW_HEIGHT,
	                              SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN |
	                              SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
	if (!_state.win) {
		log_error("SDL window creation failed: %s", SDL_GetError());
		return EIO;
	}

	/* Create event buffer to enqueue SDL events */
	if (!_state.evt_buf) {
		_state.evt_buf = hydra_buf_init(sizeof(SDL_Event), EVT_BUF_DEPTH);

		__atomic_add_fetch(&_state.evt_buf_refcount, 1, __ATOMIC_SEQ_CST);
		if (!_state.evt_buf) {
			log_error("Event buffer creation failed");
			return ENOMEM;
		}
	}

	/* Create thread that conusmes SDL events and publishes them to a hydra */
	if (pthread_create(&_state.tid, NULL, sdl_event_producer, _state.evt_buf)) {
		return ECHILD;
	}

	return 0;
}

static int
sdl_deinit(void)
{
	SDL_Event evt_quit = {.type = SDL_QUIT};

	if (!_state.win) return EINVAL;

	/* Quit SDL */
	SDL_PushEvent(&evt_quit);
	SDL_DestroyWindow(_state.win);
	SDL_Quit();
	_state.win = NULL;

	/* Wait for event producer thread to process the exit request */
	pthread_join(_state.tid, NULL);

	return 0;
}

static void*
sdl_event_producer(void *args)
{
	struct hydra_buf *evt_buf = (struct hydra_buf*)args;
	int running = 1;
	SDL_Event evt;

	while (running) {
		SDL_WaitEvent(&evt);

		switch(evt.type) {
		case SDL_QUIT:
			running = 0;
			break;
		default:
			hydra_buf_push(evt_buf, &evt);
			break;
		}

	}

	if (__atomic_sub_fetch(&_state.evt_buf_refcount, 1, __ATOMIC_SEQ_CST) <= 0) {
		/* Free event parser */
		hydra_buf_deinit(_state.evt_buf);
		_state.evt_buf = NULL;
	}

	return NULL;
}
/* }}} */
