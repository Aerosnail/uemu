#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>

#include "hydrabuffer.h"
#include "utils.h"

struct hydra_buf_consumer {
	struct hydra_buf *parent;

	size_t tail;
	sem_t used_count;
};

struct hydra_buf {
	void *data;
	size_t size;
	size_t count;
	size_t head;

	int eos;

	struct hydra_buf_consumer **consumers;
	pthread_mutex_t consumers_mtx;
	size_t consumer_count;

	sem_t free_count;
	pthread_mutex_t free_count_mtx;
};

static size_t _distance(size_t tail, size_t head, size_t count);
static void hydra_buf_pop_internal(struct hydra_buf_consumer *handle, void *dst);

struct hydra_buf*
hydra_buf_init(size_t elem_size, size_t elem_count)
{
	struct hydra_buf *handle;

	handle = calloc(1, sizeof(*handle));
	if (!handle) return NULL;

	handle->size = elem_size;
	handle->count = elem_count;
	handle->data = calloc(elem_count, elem_size);
	if (!handle->data) {
		goto failure_post_alloc;
	}

	if (sem_init(&handle->free_count, 0, elem_count) < 0) {
		goto failure_post_data_alloc;
	}

	if (pthread_mutex_init(&handle->free_count_mtx, NULL) < 0) {
		goto failure_post_sem_init;
	}

	handle->consumer_count = 0;
	if (pthread_mutex_init(&handle->consumers_mtx, NULL) < 0) {
		goto failure_post_mutex_init;
	}

	return handle;

failure_post_mutex_init:
	pthread_mutex_destroy(&handle->free_count_mtx);
failure_post_sem_init:
	sem_destroy(&handle->free_count);
failure_post_data_alloc:
	free(handle->data);
failure_post_alloc:
	free(handle);
	return NULL;
}

int
hydra_buf_deinit(struct hydra_buf *handle)
{
	size_t i;
	if (!handle) return EINVAL;

	handle->eos = 1;
	for (i=0; i<handle->consumer_count; i++) {
		sem_post(&handle->consumers[i]->used_count);
	}

	for (i=0; i<handle->consumer_count; i++) {
		hydra_buf_del_consumer(handle->consumers[i]);
	}

	pthread_mutex_destroy(&handle->consumers_mtx);
	pthread_mutex_destroy(&handle->free_count_mtx);
	sem_destroy(&handle->free_count);
	free(handle->consumers);
	free(handle->data);
	free(handle);

	return 0;
}

struct hydra_buf_consumer*
hydra_buf_add_consumer(struct hydra_buf *handle)
{
	struct hydra_buf_consumer *consumer;

	if (!handle) return NULL;

	pthread_mutex_lock(&handle->consumers_mtx);

	consumer = calloc(1, sizeof(*handle->consumers[0]));

	consumer->parent = handle;
	consumer->tail = 0;
	sem_init(&consumer->used_count, 0, 0);

	handle->consumer_count++;
	handle->consumers = realloc(handle->consumers,
	                            handle->consumer_count * sizeof(*handle->consumers));
	handle->consumers[handle->consumer_count - 1] = consumer;

	pthread_mutex_unlock(&handle->consumers_mtx);

	return consumer;
}

int
hydra_buf_del_consumer(struct hydra_buf_consumer* consumer)
{
	struct hydra_buf *handle;
	size_t i;
	int ret;

	if (!consumer) return EINVAL;

	handle = consumer->parent;

	assert(handle);
	assert(handle->consumer_count);

	pthread_mutex_lock(&handle->consumers_mtx);

	/* Look for the consumer in the parent list */
	for (i=0; i<handle->consumer_count && handle->consumers[i] != consumer; i++)
		;

	if (i < handle->consumer_count) {
		assert(handle->consumers[handle->consumer_count - 1]);

		/* Replace consumer by picking the last item in the list */
		handle->consumers[i] = handle->consumers[handle->consumer_count - 1];
		handle->consumer_count--;

		sem_destroy(&consumer->used_count);
		free(consumer);
		ret = 0;
	} else {
		ret = EBADSLT;
	}

	pthread_mutex_unlock(&handle->consumers_mtx);

	return ret;
}

int
hydra_buf_push(struct hydra_buf *handle, const void *element)
{
	uint8_t *data;
	size_t i;

	if (!handle) return EINVAL;
	if (!element) return EINVAL;

	data = handle->data;

	if (handle->eos) return EINTR;
	sem_wait(&handle->free_count);
	if (handle->eos) return EINTR;

	/* Append item and advance head */
	memcpy(data + handle->size * handle->head, element, handle->size);
	handle->head = (handle->head + 1) % handle->count;

	/* Notify consumers */
	for (i=0; i<handle->consumer_count; i++) {
		assert(handle->consumers[i]);
		sem_post(&handle->consumers[i]->used_count);
	}

	return 0;
}

int
hydra_buf_push_eos(struct hydra_buf *handle)
{
	size_t i;

	if (!handle) return EINVAL;

	handle->eos = 1;

	/* Notify consumers */
	for (i=0; i<handle->consumer_count; i++) {
		assert(handle->consumers[i]);
		sem_post(&handle->consumers[i]->used_count);
	}

	return 0;
}

int
hydra_buf_pop(struct hydra_buf_consumer *handle, void *dst)
{
	if (!handle) return EINVAL;

	if (handle->parent->eos) return EINTR;
	sem_wait(&handle->used_count);
	if (handle->parent->eos) return EINTR;

	hydra_buf_pop_internal(handle, dst);

	return 0;
}

int
hydra_buf_pop_nonblocking(struct hydra_buf_consumer *handle, void *dst)
{
	if (!handle) return EINVAL;

	if (handle->parent->eos) return EINTR;
	if (sem_trywait(&handle->used_count) < 0) return EAGAIN;
	if (handle->parent->eos) return EINTR;

	hydra_buf_pop_internal(handle, dst);

	return 0;
}

/* Static functions {{{ */
static size_t
_distance(size_t tail, size_t head, size_t count)
{
	return (tail + count - head) % count;
}

static void
hydra_buf_pop_internal(struct hydra_buf_consumer *handle, void *dst)
{
	uint8_t *data;
	size_t i;
	struct hydra_buf *parent;
	size_t our_distance, distance;
	int update_free_count;

	assert(handle->parent);
	parent = handle->parent;
	data = parent->data;

	/* Fetch item and advance tail */
	memcpy(dst, data + parent->size * handle->tail, parent->size);
	handle->tail = (handle->tail + 1) % parent->count;

	/* Check if this tail is the one furthest behind */
	our_distance = _distance(handle->tail, parent->head, parent->count);

	pthread_mutex_lock(&parent->free_count_mtx);

	update_free_count = 1;
	for (i=0; i<parent->consumer_count && update_free_count; i++) {
		assert(parent->consumers[i]);

		distance = _distance(parent->consumers[i]->tail, parent->head, parent->count);
		if (distance > our_distance) {
			update_free_count = 0;
		}
	}

	if (update_free_count) {
		sem_post(&parent->free_count);
	}

	pthread_mutex_unlock(&parent->free_count_mtx);
}
/* }}} */
