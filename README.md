uemu: an embedded system emulator
=================================

This is a free, open-source emulator targeting embedded systems. Given a
devicetree describing the hardware, it will instantiate modules to emulate each
peripheral.

The core emulator in this repo only provides a simple prompt to interact with
the system being emulated, as well as facilities to instantiate the architecture
to emulate. The actual emulation is implemented by runtime-loadable modules,
ranging from microprocessors to peripherals to IO devices.
