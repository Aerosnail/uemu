#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <core/command.h>
#include <core/clock.h>
#include <core/memory.h>
#include <core/interrupts.h>
#include <core/ui/ui.h>
#include <log/log.h>
#include <utils.h>

#include "commands/debug/dump.h"
#include "commands/debug/examine.h"
#include "commands/debug/load.h"
#include "commands/debug/write.h"
#include "commands/modules/create.h"
#include "commands/modules/gpio.h"
#include "commands/modules/insmod.h"
#include "commands/loglevel.h"
#include "dtb/parser.h"
#include "modules/manager.h"

#include "vendor/isocline/include/isocline.h"

#define SHORTOPTS "i:d:m:hv"
#define MAX_ARGCOUNT 32
#define CMD_HISTORY_LEN 256

static void version(void);
static void help(const char *exec_name);
static int cmd_init(CmdContext *ctx);
static int cmd_deinit(void);

static struct option longopts[] = {
	{ "dtb",        1, NULL, 'd' },
	{ "initscript", 1, NULL, 'i' },
	{ "loglevel",   1, NULL, 'l' + 256},
	{ "modlist",    1, NULL, 'm' },
	{ "help",       0, NULL, 'h' },
	{ "version",    0, NULL, 'v' },
	{ NULL,         0, NULL,  0  }
};

struct main_cfg {
	const char *mod_list_fname;
	const char *initscript_fname;
	const char *dtb_fname;
	enum loglevel loglevel;
};

int
main(int argc, char *argv[])
{
	CmdContext ctx;
	const char *cmd_argv[MAX_ARGCOUNT];
	int cmd_argc;
	char prompt[128];
	char *cmd;
	int c;
	int retval;
	struct main_cfg cfg = {
#ifndef NDEBUG
		.loglevel = LOG_DEBUG,
#else
		.loglevel = LOG_INFO,
#endif
	};

	mem_init();
	clock_mgr_init();
	int_mgr_init();
	module_mgr_init();
	cmd_init(&ctx);
	ui_init();


	/* Parse command line args {{{ */
	while ((c = getopt_long(argc, argv, SHORTOPTS, longopts, NULL)) != -1) {
		switch (c) {
		case 'd':
			cfg.dtb_fname = optarg;
			break;

		case 'i':
			cfg.initscript_fname = optarg;
			break;

		case 'l' + 256:
			if (!strncmp(optarg, "err", 3)) cfg.loglevel = LOG_ERROR;
			else if (!strncmp(optarg, "warn", 4)) cfg.loglevel = LOG_WARN;
			else if (!strncmp(optarg, "info", 4)) cfg.loglevel = LOG_INFO;
			else if (!strcmp(optarg, "debug")) cfg.loglevel = LOG_DEBUG;
			else {
				log_error("Unknown loglevel: %s", optarg);
				help(argv[0]);
				return EINVAL;
			}
			break;

		case 'm':
			cfg.mod_list_fname = optarg;
			break;

		case 'v':
			version();
			return 0;

		case 'h':
			help(argv[0]);
			return 0;
		}
	}
	/* }}} */

	log_set_level(cfg.loglevel);

	/* Load specified modules */
	if (cfg.mod_list_fname) {
		FILE *fd;
		char path[1024];

		log_debug("Reading module list from %s", cfg.mod_list_fname);

		if (!(fd = fopen(cfg.mod_list_fname, "r"))) {
			log_error("%s not found", cfg.mod_list_fname);
			return ENOENT;
		}

		while (fgets(path, sizeof(path), fd)) {
			if (path[strlen(path) - 1] == '\n') path[strlen(path) - 1] = 0;
			module_mgr_register(module_load(path));
		}
		fclose(fd);

	}

	/* Instantiate hardware according to devicetree */
	if (cfg.dtb_fname) {
		int fd;
		struct stat info;
		uint8_t *dtb_data;

		log_debug("Loading DTB %s", cfg.dtb_fname);

		if ((fd = open(cfg.dtb_fname, O_RDONLY)) < 0) {
			log_error("%s not found", cfg.mod_list_fname);
			return ENOENT;
		}

		fstat(fd, &info);

		dtb_data = mmap(NULL, info.st_size, PROT_READ, MAP_SHARED, fd, 0);
		dtb_load(dtb_data, info.st_size);
		munmap(dtb_data, info.st_size);

		close(fd);
	}

	/* Initialize command context with the main memory bus (or a new bus if
	 * none was created yet) */
	ctx.memory = mem_get_bus("");
	if (!ctx.memory) {
		log_error("No default memory bus");
		return ENOENT;
	}

	/* Execute commands in init script */
	if (cfg.initscript_fname) {
		FILE *fd;
		char cmd[256];

		log_debug("Executing init script %s...", cfg.initscript_fname);

		if (!(fd = fopen(cfg.initscript_fname, "r"))) {
			log_warn("Failed to open init script");
		} else {
			while (fgets(cmd, sizeof(cmd), fd)) {
				if (cmd[strlen(cmd) - 1] == '\n') cmd[strlen(cmd) - 1] = 0;
				log_debug("%s", cmd);
				cmd_argc = to_argc_argv(cmd_argv, cmd, LEN(cmd_argv));
				retval = cmd_execute(cmd_argc, (const char**)cmd_argv);

			}
			fclose(fd);
		}
	}

	/* Configure isocline */
	ic_set_history(NULL, CMD_HISTORY_LEN);

	sprintf(prompt, "[ansi-green]uemu");

	/* Parse commands */
	char *prev_cmd = strdup("");
	while ((cmd = ic_readline(prompt)) != NULL) {
		/* Enter executes the last command */
		if (cmd[0] == '\0') cmd = strdup(prev_cmd);
		free(prev_cmd);
		prev_cmd = strdup(cmd);

		/* Parse into argc/argv and execute */
		cmd_argc = to_argc_argv(cmd_argv, cmd, LEN(cmd_argv));
		retval = cmd_execute(cmd_argc, (const char**)cmd_argv);

		if (retval) {
			sprintf(prompt, "[ansi-red](%s)[ansi-green] uemu", strerror(retval));
		} else {
			sprintf(prompt, "[ansi-green]uemu");
		}
		free(cmd);
	}
	free(prev_cmd);

	ui_deinit();
	module_mgr_deinit();
	cmd_deinit();
	int_mgr_deinit();
	mem_deinit();
	clock_mgr_deinit();

	return 0;
}

static void
version(void)
{
	printf("uemu v"VERSION"\n");
}

static void
help(const char *exec_name)
{
	printf("Usage: %s [options]\n", exec_name);
	printf(
	        "   -h, --help                   Print this help screen\n"
	        "   -v, --version                Print version info\n"
		  );
}

static int
cmd_init(CmdContext *ctx)
{
	int ret;
	const char *help = "Show help info for a given command\n"
	                   "\n"
	                   "Usage: help <command>\n";

	ret = cmd_register("help", help, (cmd_fcn_t)&cmd_show_help, ctx);

	ret |= write_cmd_init(ctx);
	ret |= load_cmd_init(ctx);
	ret |= examine_cmd_init(ctx);
	ret |= insmod_cmd_init();
	ret |= create_cmd_init(ctx);
	ret |= gpio_cmd_init(ctx);
	ret |= loglevel_cmd_init();

	return ret;
}

static int
cmd_deinit(void)
{
	int ret;

	ret = cmd_unregister("help");

	ret |= write_cmd_deinit();
	ret |= load_cmd_deinit();
	ret |= examine_cmd_deinit();
	ret |= insmod_cmd_deinit();
	ret |= create_cmd_deinit();
	ret |= gpio_cmd_deinit();
	ret |= loglevel_cmd_deinit();

	return ret;
}
