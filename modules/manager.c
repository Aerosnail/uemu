#define _XOPEN_SOURCE 500
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <log/log.h>

#include "loader.h"
#include "manager.h"

typedef struct {
	Module *module;
	void *ctx;
	char *name;
} Instance;


struct {
	Module **modules;
	size_t mod_count;

	Instance *instances;
	size_t inst_count;
} _state;

int
module_mgr_init(void)
{
	_state.modules = NULL;
	_state.mod_count = 0;

	_state.instances = NULL;
	_state.inst_count = 0;

	return 0;
}

int
module_mgr_register(Module *mod)
{
	if (!mod) return EINVAL;

	_state.mod_count++;
	_state.modules = realloc(_state.modules, sizeof(*_state.modules) * _state.mod_count);
	_state.modules[_state.mod_count - 1] = mod;

	return 0;
}

int
module_mgr_unregister(const char *mod_name)
{
	Instance *instance;
	Module *module;
	size_t i;

	/* Delete any remaining instances */
	for (i=0; i<_state.inst_count; i++) {
		instance = &_state.instances[i];
		module = _state.instances[i].module;

		if (!strcmp(module_get_name(module), mod_name)) {
			log_info("Deallocating instance %s (module %s)", instance->name, mod_name);

			module_delete_instance(instance->module, instance->ctx);
			free(instance->name);

			/* Resize instances array */
			_state.instances[i] = _state.instances[_state.inst_count - 1];
			_state.inst_count--;
			if (_state.inst_count) {
				_state.instances = realloc(_state.instances,
				                           sizeof(*_state.instances) * _state.inst_count);
			} else {
				free(_state.instances);
				_state.instances = NULL;
			}

			/* Re-check the current index, since the ptr was changed */
			i--;
		}
	}

	/* Unregister module */
	for (i=0; i<_state.mod_count; i++) {
		module = _state.modules[i];
		if (!strcmp(module_get_name(module), mod_name)) {
			module_unload(module);

			_state.modules[i] = _state.modules[_state.mod_count - 1];
			_state.mod_count--;
			if (_state.mod_count) {
				_state.modules = realloc(_state.modules,
				                         sizeof(*_state.modules) * _state.mod_count);
			} else {
				free(_state.modules);
				_state.modules = NULL;
			}

			return 0;
		}
	}

	log_error("Failed to unload module %s: not found");
	return ENODATA;
}

int
module_mgr_deinit(void)
{
	Instance *instance;
	Module *module;
	size_t i;
	int ret;

	/* Delete any remaining instances */
	for (i=0; i<_state.inst_count; i++) {
		instance = &_state.instances[i];
		module = _state.instances[i].module;

		if ((ret = module_delete_instance(module, instance->ctx))) {
			log_warn("Deinit failed: %s", strerror(ret));
		}
		free(instance->name);
	}

	free(_state.instances);
	_state.instances = NULL;
	_state.inst_count = 0;

	/* Unregister modules */
	for (i=0; i<_state.mod_count; i++) {
		module = _state.modules[i];
		ret |= module_unload(module);

	}

	free(_state.modules);
	_state.modules = NULL;
	_state.mod_count = 0;

	return ret;
}

Module*
module_mgr_get_from_compatible(const char **compatibles)
{
	size_t i, j;
	const char *compatible;

	if (!compatibles) return NULL;

	/* Go through the provided "compatible" strings in order */
	for (j=0; compatibles[j]; j++) {
		compatible = compatibles[j];

		for (i=0; i<_state.mod_count; i++) {
			if (module_is_compatible(_state.modules[i], compatible)) {
				return _state.modules[i];
			}
		}
	}

	return NULL;
}

int
module_mgr_create_instance(Module *module, ModuleSpecs *specs)
{
	size_t i;
	void *ctx;
	int ret;

	/* Ensure that a module with the same name does not exist */
	for (i=0; i<_state.inst_count; i++) {
		if (!strcmp(_state.instances[i].name, specs->name)) {
			log_warn("Instance %s already exists", specs->name);
			return EADDRINUSE;
		}
	}

	ret = module_create_instance(&ctx, module, specs);
	if (ret) {
		log_error("Failed to create instance of module %s: %s",
		            module_get_name(module), strerror(ret));
		return ENOMEM;
	}

	_state.inst_count++;
	_state.instances = realloc(_state.instances,
	                           sizeof(*_state.instances) * _state.inst_count);

	_state.instances[_state.inst_count - 1].module = module;
	_state.instances[_state.inst_count - 1].ctx = ctx;
	_state.instances[_state.inst_count - 1].name = strdup(specs->name);
	return 0;
}

int
module_mgr_delete_instance(const char *mod_name, const char *inst_name)
{
	Instance *instance;
	Module *module;
	size_t i;

	for (i=0; i<_state.inst_count; i++) {
		instance = &_state.instances[i];
		module = _state.instances[i].module;

		if (!strcmp(instance->name, inst_name) && !strcmp(module_get_name(module), mod_name)) {
			log_info("Deallocating instance %s (module %s)", inst_name, mod_name);


			module_delete_instance(instance->module, instance->ctx);

			free(_state.instances[i].name);

			/* Resize instances array */
			_state.instances[i] = _state.instances[_state.inst_count - 1];
			_state.inst_count--;
			if (_state.inst_count) {
				_state.instances = realloc(_state.instances,
				                         sizeof(*_state.instances) * _state.inst_count);
			} else {
				free(_state.instances);
				_state.instances = NULL;
			}

			return 0;
		}
	}

	log_error("Instance %s of module %s not found", inst_name, mod_name);
	return ENOKEY;
}
