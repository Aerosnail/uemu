#pragma once

#include "loader.h"
#include "core/module.h"

int module_mgr_init(void);
int module_mgr_deinit(void);

int module_mgr_register(Module *mod);
int module_mgr_unregister(const char *mod_name);

Module* module_mgr_get_from_compatible(const char **compatibles);
int module_mgr_create_instance(Module *module, ModuleSpecs *specs);
int module_mgr_delete_instance(const char *mod_name, const char *inst_name);

