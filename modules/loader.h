#pragma once

#include "core/module.h"

typedef struct module Module;

Module* module_load(const char *path);
int module_unload(Module *module);

int module_parse_dtb_prop(Module *mod, void **dst, const struct dtb_property *prop, find_phandle_t find_phandle, void *find_phandle_ctx);
int module_create_instance(void **instance, Module *mod, ModuleSpecs *specs);
int module_delete_instance(Module *mod, void *instance);

const char *module_get_name(const Module *module);
int module_is_compatible(const Module *module, const char *compat_str);
