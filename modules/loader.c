#include <assert.h>
#include <dlfcn.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "core/command.h"
#include "loader.h"
#include "log/log.h"

struct module {
	ModuleInfo *info;

	int (*init)(void);
	int (*parse_dtb_prop)(void**, const struct dtb_property*, find_phandle_t, void*);
	int (*create_instance)(void** instance, ModuleSpecs *ctx);
	int (*delete_instance)(void *ctx);
	int (*deinit)(void);

	void *handle;
};

Module*
module_load(const char *path)
{
	Module *mod = calloc(1, sizeof(*mod));

	assert(mod);

	/* Load library */
	mod->handle = dlopen(path, RTLD_NOW | RTLD_LOCAL);
	if (!mod->handle) {
		log_error("Failed to load module %s", path);
		goto failure;
	}

#pragma GCC diagnostic push
/* conversion of object pointer to function pointer type */
#pragma GCC diagnostic ignored "-Wpedantic"
	/* Find symbols */
	mod->info = (ModuleInfo*) dlsym(mod->handle, "__info");
	mod->init = (int(*)(void)) dlsym(mod->handle, "__init");
	mod->parse_dtb_prop = (int(*)(void**, const struct dtb_property*, find_phandle_t, void*)) dlsym(mod->handle, "__parse_dtb_prop");
	mod->create_instance = (int(*)(void**, ModuleSpecs*)) dlsym(mod->handle, "__create_instance");
	mod->delete_instance = (int(*)(void*)) dlsym(mod->handle, "__delete_instance");
	mod->deinit = (int(*)(void)) dlsym(mod->handle, "__deinit");
#pragma GCC diagnostic pop

	if (!mod->info || !mod->init || !mod->deinit || !mod->create_instance ||
        !mod->delete_instance) {
		log_error("Required functions missing in module, not attempting to load");
		dlclose(mod->handle);
		goto failure;
	}

	log_info("Loaded module %s, version %d.%d.%d",
			mod->info->name,
			mod->info->major,
			mod->info->minor,
			mod->info->patch
	);

	/* Initialize module */
	mod->init();

	return mod;

failure:
	free(mod);
	return NULL;
}

int
module_unload(Module *mod)
{
	assert(mod);
	assert(mod->handle);

	log_debug("Unloading module %s", mod->info->name);

	mod->deinit();

	dlclose(mod->handle);
	free(mod);

	return 0;
}

int
module_parse_dtb_prop(Module *mod, void **dst, const struct dtb_property *prop, find_phandle_t find_phandle, void *find_phandle_ctx)
{
	if (mod->parse_dtb_prop) {
		return mod->parse_dtb_prop(dst, prop, find_phandle, find_phandle_ctx);
	}

	return ENOSYS;
}

int
module_create_instance(void **instance, Module *mod, ModuleSpecs *specs)
{
	return mod->create_instance(instance, specs);
}

int
module_delete_instance(Module *mod, void *instance)
{
	return mod->delete_instance(instance);
}

int
module_is_compatible(const Module *module, const char *compat_str)
{
	int i;

	if (!compat_str) return 0;
	if (!module->info->compatible) return 0;

	for (i=0; module->info->compatible[i]; i++) {
		if (!strcmp(compat_str, module->info->compatible[i])) {
			return 1;
		}
	}

	return 0;
}

const char*
module_get_name(const Module *module)
{
	return module->info->name;
}

