#pragma once

#include "memory.h"
#include "module.h"

struct mirror {
	MemBus *bus;
	MemEntry *entry;

	size_t target_addr;
};

int mirror_init(struct mirror **dst, MemBus *bus, uint32_t addr, uint32_t len, uint32_t target_addr, uint32_t target_len);
int mirror_deinit(struct mirror *self);
