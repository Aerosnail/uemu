#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include <log/log.h>
#include <module.h>
#include <utils.h>

#include "mirror.h"

static const char *_compatible[] = {
	"mirror",
	NULL
};

__global
ModuleInfo __info = {
	.name = "mirror",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible,
};

__global int
__init(void)
{
	return 0;
}

__global int
__create_instance(void **dst, ModuleSpecs *specs)
{
	struct mirror *self;
	uint32_t addr, len;
	uint32_t target_addr, target_len;
	int ret;

	addr = specs->reg[0].address;
	len = specs->reg[0].size;
	target_addr = specs->reg[1].address;
	target_len = specs->reg[1].size;

	ret = mirror_init(&self, specs->bus, addr, len, target_addr, target_len);
	if (ret) return ret;

	*dst = self;
	return 0;
}

__global int
__delete_instance(void *ctx)
{
	struct mirror *self = (struct mirror*)ctx;
	int ret = 0;

	ret |= mirror_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}
