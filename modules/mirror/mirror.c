#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "log/log.h"
#include "memory.h"

#include "mirror.h"

static int mirror_read(void *ctx, uint64_t addr, void *data, size_t len);
static int mirror_write(void *ctx, uint64_t addr, const void *data, size_t len);
static int mirror_execute(void *ctx, uint64_t addr, void *data, size_t len);

int
mirror_init(struct mirror **dst, MemBus *bus, uint32_t addr, uint32_t len, uint32_t target_addr, uint32_t target_len)
{
	struct mirror *self;
	struct mem_handler handler;

	if (target_len != len) return EINVAL;

	self = calloc(1, sizeof(*self));
	if (!self) return  ENOMEM;

	self->bus = bus;
	self->target_addr = target_addr;

	handler.read = mirror_read;
	handler.write = mirror_write;
	handler.execute = mirror_execute;
	handler.ctx = self;

	self->entry = mem_register(self->bus, &handler, addr, len);

	*dst = self;
	return 0;
}

int
mirror_deinit(struct mirror *self)
{
	mem_unregister(self->bus, self->entry);
	free(self);
	return 0;
}

/* Static functions {{{ */
static int
mirror_read(void *v_args, uint64_t offset, void *data, size_t len)
{
	struct mirror *self = (struct mirror*)v_args;
	return mem_read(self->bus, self->target_addr + offset, data, len);
}

static int
mirror_write(void *v_args, uint64_t offset, const void *data, size_t len)
{
	struct mirror *self = (struct mirror*)v_args;
	return mem_write(self->bus, self->target_addr + offset, data, len);
}

static int
mirror_execute(void *v_args, uint64_t offset, void *data, size_t len)
{
	struct mirror *self = (struct mirror*)v_args;
	return mem_execute(self->bus, self->target_addr + offset, data, len);
}
/* }}} */
