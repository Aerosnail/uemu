#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include <log/log.h>
#include <module.h>
#include <utils.h>

#include "ram.h"

static const char *_compatible[] = {
	"ram",
	"memory",
	"mmio-sram",
	"soc-nv-flash",
	NULL
};

__global
ModuleInfo __info = {
	.name = "ram",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible,
};

__global int
__init(void)
{
	return 0;
}

__global int
__create_instance(void **dst, ModuleSpecs *specs)
{
	struct ram *self;
	uint32_t addr, len;
	int ret;

	addr = specs->reg->address;
	len = specs->reg->size;

	ret = ram_init(&self, specs->bus, len);
	if (ret) return ret;

	ret = ram_map(self, addr, len);
	if (ret) return ret;

	*dst = self;
	return 0;
}

__global int
__delete_instance(void *ctx)
{
	struct ram *self = (struct ram*)ctx;
	int ret = 0;

	ret |= ram_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}
