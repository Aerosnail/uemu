#pragma once

#include "memory.h"
#include "module.h"

struct ram {
	MemBus *bus;
	MemEntry *entry;

	void *mem;
	size_t len;
};

int ram_init(struct ram **dst, MemBus *bus, size_t len);
int ram_map(struct ram *self, uint64_t addr, size_t len);
int ram_deinit(struct ram *self);
