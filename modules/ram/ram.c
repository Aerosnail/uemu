#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "log/log.h"
#include "memory.h"

#include "ram.h"

static int ram_read(void *ctx, uint64_t addr, void *data, size_t len);
static int ram_write(void *ctx, uint64_t addr, const void *data, size_t len);
static int ram_execute(void *ctx, uint64_t addr, void *data, size_t len);

static inline void static_memcpy(void *restrict dst, const void *restrict src, size_t len);

int
ram_init(struct ram **dst, MemBus *bus, size_t len)
{
	struct ram *self = calloc(1, sizeof(*self));

	if (!self) return ENOMEM;

	self->bus = bus;
	self->len = len;
	self->mem = calloc(len, 1);
	if (!self->mem) return ENOMEM;

	*dst = self;
	return 0;
}

int
ram_map(struct ram *self, uint64_t addr, size_t len)
{
	struct mem_handler handler;

	handler.read = ram_read;
	handler.write = ram_write;
	handler.execute = ram_execute;
	handler.ctx = self;

	self->entry = mem_register(self->bus, &handler, addr, len);

	return 0;
}

int
ram_deinit(struct ram *self)
{
	mem_unregister(self->bus, self->entry);

	free(self->mem);
	free(self);
	return 0;
}

/* Static functions {{{ */
static int
ram_read(void *v_args, uint64_t offset, void *data, size_t len)
{
	struct ram *args = (struct ram*)v_args;

	assert(offset + len <= args->len);
	static_memcpy(data, (uint8_t *restrict)args->mem + offset, len);

	return 0;
}

static int
ram_write(void *v_args, uint64_t offset, const void *data, size_t len)
{
	struct ram *args = (struct ram*)v_args;

	assert(offset + len <= args->len);
	static_memcpy((uint8_t *restrict)args->mem + offset, data, len);

	return 0;
}

static int
ram_execute(void *v_args, uint64_t offset, void *data, size_t len)
{
	struct ram *args = (struct ram*)v_args;

	assert(offset + len <= args->len);
	static_memcpy(data, (uint8_t *restrict)args->mem + offset, len);

	return 0;
}

inline static void
static_memcpy(void *restrict dst, const void *restrict src, size_t len)
{
	size_t i;

	for (i = 0; i < len; i++) {
		((uint8_t *restrict)dst)[i] = ((uint8_t *restrict)src)[i];
		__asm(""); /* Prevents this from turning into a memcpy */
	}
}
/* }}} */
