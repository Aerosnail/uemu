#include <assert.h>

#include <module.h>

#include "commands.h"
#include "template.h"

static const char *_compatible[] = {
	NULL,
};

__global
ModuleInfo __info = {
	.name = "template",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible
};

__global int
__init(void)
{
	return 0;
}

__global int
__create_instance(void **dst, DTBModuleSpecs *specs)
{
	struct template *self;
	int ret;

	ret = template_init(&self, specs->bus);
	if (ret) return ret;

	ret = register_commands(name, self);
	if (ret) return ret;

	*dst = self;
	return self;
}

__global int
__delete_instance(void *ctx)
{
	int ret = 0;
	struct template *self = (struct template*)ctx;

	ret |= unregister_commands(name);
	ret |= template_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}
