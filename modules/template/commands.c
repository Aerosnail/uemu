#include <command.h>

#include <utils.h>

#include "commands.h"

typedef struct {
	const char *cmd;
	const char *help;
	int (*fcn)(int, const char*[], void*);
} command_t;


static const command_t cmds[] = {
};

int
register_commands(const char *name, struct template *ctx)
{
	int ret = 0;
	size_t i;

	for (i=0; i<LEN(cmds); i++) {
		ret |= cmd_register_module(name,
				cmds[i].cmd, cmds[i].help, cmds[i].fcn, ctx);
	}

	return ret;
}

int
unregister_commands(const char *name)
{
	return cmd_unregister_module(name, NULL);
}
