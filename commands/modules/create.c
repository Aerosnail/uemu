#include <errno.h>
#include <module.h>

#include <core/command.h>
#include <log/log.h>

#include "create.h"
#include "modules/manager.h"

static int create(int argc, const char *argv[], void *ctx);

int
create_cmd_init(CmdContext *ctx)
{
	int ret = 0;
	const char *help_create = "Create a new instance of a module\n"
	                          "\n"
	                          "Usage: reatea <module name> <new instance name>";
	ret |= cmd_register("create", help_create, &create, ctx);

	return ret;
}

int
create_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("create");

	return ret;
}

static int
create(int argc, const char *argv[], void *v_ctx)
{
	const char *compatible[] = {argv[2], NULL};
	CmdContext *ctx = (CmdContext*)v_ctx;
	ModuleSpecs mod_ctx = {
		.bus = ctx->memory,
		.name = argv[1],
		.compatible = argv[2],
	};

	Module *mod;
	int ret;

	if (argc < 3) {
		log_warn("Usage: create <instance name> <compatibility string>");
		return EINVAL;
	}

	mod = module_mgr_get_from_compatible(compatible);
	if (!mod) {
		log_error("No module compatible with %s found", mod_ctx.compatible);
		return ENOKEY;
	}

	ret = module_mgr_create_instance(mod, &mod_ctx);

	return ret;
}
