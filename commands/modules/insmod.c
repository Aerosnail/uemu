#include <errno.h>
#include <stdio.h>

#include <core/command.h>
#include <log/log.h>

#include "insmod.h"
#include "modules/loader.h"
#include "modules/manager.h"

static int insmod(int argc, const char *argv[], void *ctx);
static int rmmod(int argc, const char *argv[], void *ctx);

int
insmod_cmd_init(void)
{
	int ret = 0;
	const char *help_insmod = "Load a module\n"
	                          "\n"
	                          "Usage: insmod <module path.so>";
	const char *help_rmmod = "Unload a module\n"
	                         "\n"
	                         "Usage: rmmod <module name>";
	ret |= cmd_register("insmod", help_insmod, &insmod, NULL);
	ret |= cmd_register("rmmod", help_rmmod, &rmmod, NULL);

	return ret;
}

int
insmod_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("insmod");
	ret |= cmd_unregister("rmmod");

	return ret;
}

static int
insmod(int argc, const char *argv[], void *ctx)
{
	(void)ctx;

	if (argc < 2) {
		log_warn("Usage: insmod <moudle path.so>");
		return EINVAL;
	}

	return module_mgr_register(module_load(argv[1]));
}

static int
rmmod(int argc, const char *argv[], void *ctx)
{
	(void)ctx;

	if (argc < 2) {
		log_warn("Usage: rmmod <module name>");
		return EINVAL;
	}

	return module_mgr_unregister(argv[1]);
}
