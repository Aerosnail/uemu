#include <errno.h>

#include <core/command.h>
#include <core/gpio.h>
#include <log/log.h>

#include "gpio.h"

static int gpio_connect_cmd(int argc, const char *argv[], void *ctx);
static int gpio_ls(int argc, const char *argv[], void *ctx);

int
gpio_cmd_init(CmdContext *ctx)
{
	int ret = 0;

	(void)ctx;

	const char *help_gpioconn = "Connect two registered GPIO pins";
	const char *help_gpiols = "List all registered GPIOs";

	ret |= cmd_register("gpio connect", help_gpioconn, &gpio_connect_cmd, NULL);
	ret |= cmd_register("gpio ls", help_gpiols, &gpio_ls, NULL);

	return ret;
}

int
gpio_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("gpio connect");
	ret |= cmd_unregister("gpio ls");

	return ret;
}

static int
gpio_connect_cmd(int argc, const char *argv[], void *ctx)
{
	(void)ctx;
	GPIO *a, *b;

	if (argc < 5) {
		log_warn("Usage: gpio connect <module 1> <gpio 1> <module 2> <gpio 2>");
		return EINVAL;
	}

	a = gpio_find_by_name(argv[1], argv[2]);
	b = gpio_find_by_name(argv[3], argv[4]);

	return gpio_connect(a, b);
}

static int
gpio_ls(int argc, const char *argv[], void *ctx)
{
	/* TODO implement */
	return ENOSYS;
}
