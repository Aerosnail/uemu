#include <stdio.h>
#include <errno.h>

#include "core/command.h"
#include "dump.h"
#include "log/log.h"

static int dump(int argc, const char *argv[], void *ctx);

int
dump_cmd_init(CmdContext *ctx)
{
	int ret = 0;
	const char *help = "Dump an arbitrary memory area to file\n"
	                   "\n"
	                   "Usage: dump <file.bin> <start address> <length>";

	ret |= cmd_register("dump", help, &dump, ctx);

	return ret;
}

int
dump_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("dump");

	return ret;
}

static int
dump(int argc, const char *argv[], void *ctx)
{
	/* TODO implement */
	return ENOSYS;
}
