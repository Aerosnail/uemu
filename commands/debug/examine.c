#include <errno.h>
#include <stdio.h>
#include <inttypes.h>

#include "utils.h"

#include "examine.h"
#include "log/log.h"

#define BYTES_PER_LINE 16

static int examine(int argc, const char *argv[], void *ctx);

int
examine_cmd_init(CmdContext *ctx)
{
	int ret = 0;
	const char *help = "Inspect a memory area\n"
	                   "\n"
	                   "Usage: examine <byte count><format specifier> <start address>";

	ret |= cmd_register("examine", help, &examine, ctx);
	ret |= cmd_register("x", help, &examine, ctx);

	return ret;
}

int
examine_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("examine");
	ret |= cmd_unregister("x");

	return ret;
}

static int
examine(int argc, const char *argv[], void *v_ctx)
{
	CmdContext *ctx = (CmdContext*)v_ctx;
	unsigned long count;
	uint64_t addr;
	char *fmt;
	unsigned long i, chunksize;
	uint8_t tmp[16];
	int ret = 0;

	if (argc < 3) {
		log_warn("Usage: examine <format> <start addr>");
		return EINVAL;
	}

	count = strtoul(argv[1], &fmt, 0);
	addr = strtoull(argv[2], NULL, 16);

	switch (*fmt) {
	case 'x':
		fmt = "%02x ";
		break;
	case 'X':
		fmt = "%02X ";
		break;
	default:
		log_error("Unrecognized format: '%c'", *fmt);
		return EINVAL;
	}

	log_debug("0x%"PRIx64"+%d", addr, count);

	while (count) {
		chunksize = MIN(sizeof(tmp), count);
		ret |= mem_read(ctx->memory, addr, tmp, chunksize);

		printf("0x%08"PRIx64" |  ", addr);

		for (i=0; i<chunksize; i++) {
			printf(fmt, tmp[i]);
		}

		count -= chunksize;
		addr += chunksize;
		printf("\n");
	}

	return ret;
}
