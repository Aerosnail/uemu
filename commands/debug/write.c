#include <errno.h>
#include <stdio.h>
#include <inttypes.h>

#include "core/command.h"
#include "write.h"
#include "log/log.h"

#define BYTES_PER_LINE 32

static int cmd_write(int argc, const char *argv[], void *ctx);

int
write_cmd_init(CmdContext *ctx)
{
	int ret = 0;
	const char *help = "Write hex bytes to memory\n"
	                   "\n"
	                   "Usage: write <start address> <hex byte 0> <hex byte 1>...";

	ret |= cmd_register("write", help, &cmd_write, ctx);
	ret |= cmd_register("w", help, &cmd_write, ctx);

	return ret;
}

int
write_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("write");
	ret |= cmd_unregister("w");

	return ret;
}

static int
cmd_write(int argc, const char *argv[], void *v_ctx)
{
	CmdContext *ctx = (void*)v_ctx;
	int i, ret;
	uint64_t addr;
	uint8_t *data;

	if (argc < 3) {
		log_warn("Usage: write <start addr> <hex bytes>");
		return EINVAL;
	}

	addr = strtoull(argv[1], NULL, 16);
	data = calloc(argc - 2, sizeof(*data));
	if (!data) return ENOMEM;

	for (i=2; i<argc; i++) {
		data[i - 2] = strtoull(argv[i], NULL, 16);
	}

	ret = mem_write(ctx->memory, addr++, data, argc - 2);

	free(data);
	return ret;
}
