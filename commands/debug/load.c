#include <errno.h>
#include <stdio.h>
#include <inttypes.h>

#include "core/command.h"
#include "load.h"
#include "log/log.h"

static int load(int argc, const char *argv[], void *ctx);

int
load_cmd_init(CmdContext *ctx)
{
	int ret = 0;
	const char *help = "Load an arbitrary file into memory\n"
	                   "\n"
	                   "Usage: load <start address> <file.bin>";

	ret |= cmd_register("load", help, &load, ctx);

	return ret;
}

int
load_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("load");

	return ret;
}

static int
load(int argc, const char *argv[], void *v_ctx)
{
	CmdContext *ctx = (CmdContext*)v_ctx;

	uint64_t addr;
	uint8_t tmp[4096];
	FILE *fd;
	size_t len;
	int ret;

	if (argc < 3) {
		log_warn("Usage: load <start address> <file.bin>");
		return EINVAL;
	}

	addr = strtoull(argv[1], NULL, 16);

	if (!(fd = fopen(argv[2], "r"))) {
		log_warn("Failed to open file %s", argv[2]);
		return ENOENT;
	}

	ret = 0;
	while ((len = fread(tmp, 1, sizeof(tmp), fd)) > 0) {
		log_debug("Writing 0x%08"PRIx64"+%d", addr, len);
		ret |= mem_write(ctx->memory, addr, tmp, len);
		addr += len;
	}
	fclose(fd);

	return ret;
}
