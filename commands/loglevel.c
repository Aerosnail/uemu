#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <log/log.h>

#include "loglevel.h"

#include <core/command.h>
#include <log/log.h>

static int loglevel_cmd(int argc, const char *argv[], void *ctx);

int
loglevel_cmd_init(void)
{
	int ret = 0;
	const char *help_loglevel = "Set current log level";

	ret |= cmd_register("loglevel", help_loglevel, &loglevel_cmd, NULL);

	return ret;
}

int
loglevel_cmd_deinit(void)
{
	int ret = 0;

	ret |= cmd_unregister("loglevel");

	return ret;
}

static int
loglevel_cmd(int argc, const char *argv[], void *ctx)
{
	(void)ctx;

	if (argc < 2) {
		log_warn("Usage: loglevel <debug|info|warn|error|none>");
		return EINVAL;
	}

	if (!strcmp(argv[1], "debug")) {
		log_set_level(LOG_DEBUG);
	} else if (!strcmp(argv[1], "info")) {
		log_set_level(LOG_INFO);
	} else if (!strcmp(argv[1], "warn")) {
		log_set_level(LOG_WARN);
	} else if (!strcmp(argv[1], "error")) {
		log_set_level(LOG_ERROR);
	} else if (!strcmp(argv[1], "none")) {
		log_set_level(LOG_NONE);
	} else {
		log_warn("Usage: loglevel <debug|info|warn|error|none>");
		return EINVAL;
	}

	return 0;
}
