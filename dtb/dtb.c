#include <errno.h>

#include "dtb.h"

void
dtb_to_host(void *v_dst, const void *v_src, size_t offset, size_t len)
{
	uint32_t *dst = (uint32_t*)v_dst;
	uint32_t *src = (uint32_t*)v_src + offset;
	size_t i;

	for (i=0; i<len; i++) {
		dst[i] = (src[i] & 0xFF) << 24
		       | (src[i] & 0xFF << 8) << 8
		       | (src[i] & 0xFF << 16) >> 8
		       | src[i] >> 24;
	}
}

int
dtb_validate_header(const struct fdt_header *header, size_t len)
{
	if (header->magic != DTB_MAGIC) {
		return EINVAL;
	}

	if (header->totalsize > len) {
		return EINVAL;
	}

	if (header->off_dt_struct > header->totalsize ||
	    header->off_dt_struct < sizeof(header) ||
	    header->off_dt_strings > header->totalsize ||
	    header->off_dt_strings < sizeof(header) ||
	    header->off_mem_rsvmap > header->totalsize ||
	    header->off_mem_rsvmap < sizeof(header)) {
		return EINVAL;
	}

	if (header->last_comp_version > DTB_SPEC_VERSION) {
		return EINVAL;
	}


	if (header->size_dt_strings + header->off_dt_strings > header->totalsize ||
	    header->size_dt_struct + header->off_dt_struct > header->off_dt_strings) {
		return EINVAL;
	}

	return 0;
}
