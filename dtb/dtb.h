#pragma once

#include <stdint.h>
#include <stdlib.h>

#define DTB_SPEC_VERSION 17

#define DTB_MAGIC 0xD00DFEED

enum fdt_block_type {
	FDT_BEGIN_NODE = 1,
	FDT_END_NODE = 2,
	FDT_PROP = 3,
	FDT_NOP = 4,
	FDT_END = 9
};

struct fdt_header {
	uint32_t magic;
	uint32_t totalsize;

	uint32_t off_dt_struct;
	uint32_t off_dt_strings;
	uint32_t off_mem_rsvmap;

	uint32_t version;
	uint32_t last_comp_version;

	uint32_t boot_cpuid_phys;

	uint32_t size_dt_strings;
	uint32_t size_dt_struct;
}__attribute__((packed));

struct fdt_prop {
	uint32_t len;
	uint32_t nameoff;
}__attribute__((packed));

void dtb_to_host(void *v_dst, const void *v_src, size_t offset, size_t len);

int dtb_validate_header(const struct fdt_header *header, size_t len);
