#pragma once

#include <stdint.h>
#include <stdlib.h>

int dtb_load(const uint8_t *dtb_data, size_t len);
