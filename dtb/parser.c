#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <gpio.h>
#include <log/log.h>
#include <module.h>
#include <interrupts.h>
#include <utils.h>
#include <dtb/node.h>

#include "modules/manager.h"
#include "parser.h"
#include "dtb.h"

struct dtb_nodes_tree {
	struct dtb_node node;
	MemBus *bus;

	struct dtb_nodes_tree *sibling;
	struct dtb_nodes_tree *child;
	struct dtb_nodes_tree *parent;
};

enum controller_prop {
	CTL_PROP_INTERRUPT = 0,
	CTL_PROP_RANGES,

	CTL_PROP_COUNT
};

static const char *_controller_props[] = {
	[CTL_PROP_INTERRUPT] = "interrupt-controller",
	[CTL_PROP_RANGES] = "ranges",
};

static const struct dtb_node* find_phandle(const struct dtb_nodes_tree *tree, uint32_t phandle);
static struct dtb_nodes_tree* add_child(struct dtb_nodes_tree *parent);
static int instantiate_node(struct dtb_node *node, MemBus *bus, struct dtb_nodes_tree *root);
static int check_for_controllers(struct dtb_nodes_tree *leaf, struct dtb_property *prop);

int
dtb_load(const uint8_t *dtb_data, size_t len)
{
	struct fdt_header header;
	struct fdt_prop property;
	size_t offset;
	uint32_t token_type;
	const char *node_name, *prop_name;
	const uint8_t *structure_block;
	const char *strings_block;

	struct dtb_nodes_tree *root = NULL;
	struct dtb_nodes_tree *leaf = NULL;
	struct dtb_nodes_tree *next_leaf;

	struct dtb_property prop;
	int ret;

	/* Read header */
	dtb_to_host(&header, dtb_data, 0, sizeof(header)/sizeof(uint32_t));

	/* Ensure that header is consistent */
	if (dtb_validate_header(&header, len)) return EINVAL;

	structure_block = (uint8_t*)(dtb_data + header.off_dt_struct);
	strings_block   = (char*)(dtb_data + header.off_dt_strings);
	offset = 0;

	while (offset < header.size_dt_struct) {

		/* Parse next token */
		dtb_to_host(&token_type, structure_block + offset, 0, 1);
		offset += 4;

		switch (token_type) {
		case FDT_BEGIN_NODE:
			/* Parse node name, advance offset */
			node_name = (char*)structure_block + offset;
			offset += strlen(node_name) + 1;

			/* Append a new struct to populate with module properties */
			if (!root) {
				/* Root node */
				root = calloc(1, sizeof(*root));
				assert(root);
				leaf = root;

				ret = mem_create_bus(&leaf->bus, node_name);
				if (ret) return ret;
				dtb_node_init(&leaf->node, NULL);
			} else {
				/* Child of current node */
				leaf = add_child(leaf);
				assert(leaf);

				dtb_node_init(&leaf->node, &leaf->parent->node);

				if (!leaf->parent->bus) {
					ret = mem_create_bus(&leaf->parent->bus, leaf->parent->node.name);
					if (ret) return ret;
				}

			}

			leaf->node.name = node_name;
			break;

		case FDT_END_NODE:
			assert(leaf);
			leaf = leaf->parent;
			break;

		case FDT_PROP:
			assert(leaf);
			/* Parse property name, value and size */
			dtb_to_host(&property, structure_block + offset, 0, sizeof(property)/sizeof(uint32_t));
			prop_name = strings_block + property.nameoff;
			offset += sizeof(property);

			/* Populate struct with new property */
			dtb_property_init(&prop, prop_name, property.len, structure_block + offset);
			dtb_node_add_property(&leaf->node, &prop);

			/* Create controllers if specific props are found */
			check_for_controllers(leaf, &prop);

			offset += property.len;
			break;

		case FDT_NOP:
			break;

		case FDT_END:
			/* Tree depth should be 0 at this point, otherwise there are some
			 * missing FDT_END_NODE tokens */
			assert(!leaf);
			break;

		default:
			log_warn("Unrecognized token type: %"PRIu32, token_type);
			break;
		}

		/* Keep offset aligned to 32 bits */
		offset = (offset + sizeof(uint32_t) - 1) / sizeof(uint32_t) * sizeof(uint32_t);
	}

	/* Flatten tree */
	leaf = root;
	next_leaf = root;
	while (leaf->child || leaf->sibling) {
		if (leaf->child) {
			for (; next_leaf->sibling; next_leaf = next_leaf->sibling)
				;

			next_leaf->sibling = leaf->child;
		}
		leaf = leaf->sibling;
	}

	/* At this point the devicetree has been parsed into a linked list. It can
	 * be traversed entirely by only looking an the ->sibling pointer, and the
	 * parent of a given node can be accessed by looking at the ->parent
	 * property */

	/* Create instances with a phandle */
	for (leaf = root; leaf; leaf = leaf->sibling) {
		if (leaf->node.phandle && leaf->node.compatible) {
			instantiate_node(&leaf->node, leaf->parent->bus, root);
		}
	}

	/* Create instances without a phandle */
	for (leaf = root; leaf; leaf = leaf->sibling) {
		if (!leaf->node.phandle && leaf->node.compatible) {
			assert(leaf->parent);
			instantiate_node(&leaf->node, leaf->parent->bus, root);
		}
	}

	/* CLean up */
	for (leaf = root; leaf;) {
		dtb_node_deinit(&leaf->node);
		next_leaf = leaf->sibling;
		free(leaf);
		leaf = next_leaf;
	}

	return 0;
}

/* Static functions {{{ */
static struct dtb_nodes_tree*
add_child(struct dtb_nodes_tree *parent)
{
	struct dtb_nodes_tree *child;

	if (!parent) return NULL;

	child = calloc(1, sizeof(*child));

	child->parent = parent;

	if (!parent->child) {
		parent->child = child;
	} else {
		child->sibling = parent->child;
		parent->child = child;
	}

	return child;
}

static const struct dtb_node*
find_phandle(const struct dtb_nodes_tree *tree, uint32_t phandle)
{
	const struct dtb_nodes_tree *item = NULL;

	for (item = tree; item != NULL; item = item->sibling) {
		if (item->node.phandle == phandle) {
			return &item->node;
		}
	}

	return NULL;
}

static int
instantiate_node(struct dtb_node *node, MemBus *bus, struct dtb_nodes_tree *root)
{
	Module *mod;
	ModuleSpecs specs;
	size_t i;

	if (!node) return EINVAL;
	if (!node->compatible || !node->compatible[0]) return 0;

	/* Find compatible module */
	mod = module_mgr_get_from_compatible(node->compatible);
	if (!mod) {
		log_warn("No compatible module for %s found", node->name);
		return ENOKEY;
	}

	/* Initialize specs with known properties */
	specs.bus = bus;
	specs.name = node->name;
	specs.compatible = node->compatible[0];
	specs.reg = node->reg;
	specs.reg_count = node->reg_count;
	specs.ctx = NULL;

	/* Call __parse_dtb_prop for all unknown properties */
	for (i = 0; i < node->props_count; i++) {
		if (module_parse_dtb_prop(mod,
			                      &specs.ctx,
			                      &node->props[i],
			                      (find_phandle_t)find_phandle,
			                      root)) {
			log_warn("Unknown property for module %s: %s",
			         module_get_name(mod),
			         node->props[i].name);

		}
	}

	/* Instantiate */
	return module_mgr_create_instance(mod, &specs);
}

static int
check_for_controllers(struct dtb_nodes_tree *leaf, struct dtb_property *prop)
{
	enum controller_prop ctl_prop;
	for (ctl_prop = 0; ctl_prop < CTL_PROP_COUNT; ctl_prop++) {
		if (!strcmp(prop->name, _controller_props[ctl_prop])) {
			break;
		}
	}

	switch (ctl_prop) {
	case CTL_PROP_INTERRUPT:
		return int_mgr_create(leaf->node.name) == NULL;

	case CTL_PROP_RANGES:
		if (!prop->len) {
			leaf->bus = leaf->parent->bus;
		} else {
			log_warn("ranges address translation not yet supported");
		}

	default:
		break;
	}

	return 1;
}
/* }}} */
